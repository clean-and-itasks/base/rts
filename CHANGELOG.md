# Changelog

#### v3.0.3

- Enhancement: optimisation, on linux use transparent huge pages for heaps of at
  least 20 mb.
- Enhancement: several optimisations of the compacting garbage collector.
- Fix: bug in the marking collector, the counter in rbp before the
  `_push_lazy_args` loop was 1 too many for thunks with unboxed values, so the
  evaluation address was used also as a pointer to a node, but because only
  pointers inside the heap are marked and this address is outside the heap.

#### v3.0.2

- Fix: fix copying garbage collector for thunks with 1 unboxed value on x86 and
  x64.
- Fix: fix computation of number of words of the bit vector to be zeroed in the
  x64 copying collector.

#### v3.0.1

- Fix: fix profiling object files on linux-x86.

## v3.0

- Feature: add support for `{..:}` arrays which reserve the next power of 2 of
  memory.

### v2.1.0

- Feature: add `Clean.h`.

## v2.0

- Enhancement: add dedicated `_ARRAY_` descriptors for unboxed arrays of `Int`,
  `Real`, `Bool`, 32-bit `Int`, and 32-bit `Real`, to save space. Add
  `_ARRAY_R_` descriptor for unboxed arrays of records.

## v1.0

First tagged version.
