#!/bin/bash
mkdir ml64
dos2unix -n astartup_ml64.sed ml64/astartup_ml64.sed
 for asm_file in astartup acopy amark amark_prefetch acompact acompact_rmark acompact_rmark_prefetch acompact_rmarkr aap ; do
  dos2unix -n $asm_file.asm ml64/$asm_file.asm_u
  sed -r -f ml64/astartup_ml64.sed < ml64/$asm_file.asm_u > ml64/$asm_file.asm
  rm ml64/$asm_file.asm_u
  unix2dos ml64/$asm_file.asm
done
cp areals.asm ml64/areals.asm
cp aprofile.asm ml64/aprofile.asm
cp aprofilegraph.asm ml64/aprofilegraph.asm
cp atrace.asm ml64/atrace.asm

