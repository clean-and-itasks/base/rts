
; mark used nodes and pointers in argument parts and link backward pointers

	mov	rax,qword ptr heap_size_65+0
	shl	rax,6
	mov	qword ptr heap_size_64_65+0,rax 

	lea	rax,(-16000)[rsp]
	mov	qword ptr end_stack+0,rax

 ifdef GC_HOOKS
	mov	rax,qword ptr gc_hook_before_compact+0
	test	rax,rax
	je	no_gc_hook_before_compact
	call	rax
no_gc_hook_before_compact:
 endif

	mov	rax,qword ptr caf_list+0

	test	rax,rax 
	je	end_mark_cafs

mark_cafs_lp:
	push	(-8)[rax]

	lea	rsi,8[rax]
	mov	rax,qword ptr [rax]
	lea	rcx,[rsi+rax*8]

	mov	qword ptr end_vector+0,rcx 

	call	rmark_stack_nodes

	pop	rax
	test	rax,rax 
	jne	mark_cafs_lp

end_mark_cafs:
	mov	rsi,qword ptr stack_p+0

	mov	rcx,qword ptr stack_top+0
	mov	qword ptr end_vector+0,rcx 

	call	rmark_stack_nodes

	call	add_mark_compact_garbage_collect_time
	
	jmp	compact_heap

	include	acompact_rmark.asm

	include acompact_rmarkr.asm

; compact the heap

compact_heap:

 ifdef GC_HOOKS
	mov	rax,qword ptr gc_hook_between_mark_and_compact+0
	test	rax,rax
	je	no_gc_hook_between_mark_and_compact
	call	rax
no_gc_hook_between_mark_and_compact:
 endif

 ifdef PIC
	lea	rcx,finalizer_list+0
	lea	rdx,free_finalizer_list+0
 else
	mov	rcx,offset finalizer_list
	mov	rdx,offset free_finalizer_list
 endif

	mov	rbp,qword ptr [rcx]
determine_free_finalizers_after_compact1:
	lea	r9,__Nil-8+0
	cmp	rbp,r9
	je	end_finalizers_after_compact1

	mov	rax,qword ptr neg_heap_p3+0
	add	rax,rbp 
	mov	rbx,rax
	and	rax,31*8
	shr	rbx,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	esi,dword ptr [r9+rax]
 else
	mov	esi,dword ptr (bit_set_table2)[rax]
 endif
	test	esi,dword ptr [rdi+rbx*4]
	je	finalizer_not_used_after_compact1

	mov	rax,qword ptr [rbp]
	mov	rsi,rbp 
	jmp	finalizer_find_descriptor

finalizer_find_descriptor_lp:
	and	rax,-4
	mov	rsi,rax 
	mov	rax,qword ptr [rax]
finalizer_find_descriptor:
	test	rax,1
	jne	finalizer_find_descriptor_lp

 ifdef PIC
	lea	r9,e____system__kFinalizerGCTemp+2+0
	mov	qword ptr [rsi],r9
 else
	mov	qword ptr [rsi],offset e____system__kFinalizerGCTemp+2
 endif

	cmp	rbp,rcx 
	ja	finalizer_no_reverse

	mov	rax,qword ptr [rbp]
	lea	rsi,1[rcx]
	mov	qword ptr [rbp],rsi 
	mov	qword ptr [rcx],rax 

finalizer_no_reverse:
	lea	rcx,8[rbp]
	mov	rbp,qword ptr 8[rbp]
	jmp	determine_free_finalizers_after_compact1

finalizer_not_used_after_compact1:
 ifdef PIC
	lea	r9,e____system__kFinalizerGCTemp+2+0
	mov	qword ptr [rbp],r9
 else
	mov	qword ptr [rbp],offset e____system__kFinalizerGCTemp+2
 endif

	mov	qword ptr [rdx],rbp 
	lea	rdx,8[rbp]

	mov	rbp,qword ptr 8[rbp]
	mov	qword ptr [rcx],rbp 

	jmp	determine_free_finalizers_after_compact1

end_finalizers_after_compact1:
	mov	qword ptr [rdx],rbp 

	mov	rcx,qword ptr finalizer_list+0
	lea	r9,__Nil-8+0
	cmp	rcx,r9
	je	finalizer_list_empty
	test	rcx,3
	jne	finalizer_list_already_reversed
	mov	rax,qword ptr [rcx]
 ifdef PIC
	lea	r9,finalizer_list+1+0
	mov	qword ptr [rcx],r9
 else
	mov	qword ptr [rcx],offset finalizer_list+1
 endif
	mov	qword ptr finalizer_list+0,rax 
finalizer_list_already_reversed:
finalizer_list_empty:

 ifdef PIC
	lea	rsi,free_finalizer_list+0
 else
	mov	rsi,offset free_finalizer_list
 endif
	lea	r9,__Nil-8+0
	cmp	qword ptr [rsi],r9
	je	free_finalizer_list_empty

 ifdef PIC
	lea	r9,free_finalizer_list+8+0
	mov	qword ptr end_vector+0,r9
 else
	mov	qword ptr end_vector+0,offset free_finalizer_list+8
 endif

	call	rmark_stack_nodes

free_finalizer_list_empty:

	mov	rax,qword ptr heap_size_65+0
	mov	rbx,rax 
	shl	rbx,6

	add	rbx,qword ptr heap_p3+0

	mov	qword ptr end_heap_p3+0,rbx

	add	rax,3
	shr	rax,2
	mov	r12,rax

	mov	r8,qword ptr heap_vector+0

	lea	rbx,4[r8]
	neg	rbx
	mov	qword ptr neg_heap_vector_plus_4+0,rbx 

	mov	rdi,qword ptr heap_p3+0
	xor	rsi,rsi 
	jmp	find_non_zero_long

move_argument_part_b_arguments_used_once:
	mov	rbx,qword ptr (-18)[rax]
	test	rbx,1
	je	end_list_3
find_descriptor_3:
	and	rbx,-4
	mov	rbx,qword ptr [rbx]
	test	rbx,1
	jne	find_descriptor_3
end_list_3:
	movzx	rbx,word ptr (-2)[rbx]
	sub	rbx,256+3
	jmp	copy_argument_part_1r

; %rax ,%rcx ,%rbp : free
find_non_zero_long:
	mov	rbp,qword ptr neg_heap_vector_plus_4+0

skip_zeros:
	sub	r12,1
	jc	end_move
	mov	esi,dword ptr [r8]
	add	r8,4
	test	rsi,rsi 
	je	skip_zeros
; %rbp : free
end_skip_zeros:

	add	rbp,r8
	shl	rbp,6
	add	rbp,qword ptr heap_p3+0

bsf_and_copy_nodes:
 ifndef LINUX
	db	0f3h
	bsf	ecx,esi
 else
	rep bsf ecx,esi
 endif
copy_nodes:
	mov	rax,qword ptr [rbp+rcx*8]
	lea	rbx,(-1)[rsi]
	lea	rcx,8[rbp+rcx*8]
	and	rsi,rbx

	test	rax,2
	je	begin_update_list_2

	test	rax,1
	je	move_argument_part_b_arguments_used_once

move_argument_part:
	mov	rbx,qword ptr (-19)[rax]
	sub	rax,3

	test	rbx,1
	je	end_list_2
find_descriptor_2:
	and	rbx,-4
	mov	rbx,qword ptr [rbx]
	test	rbx,1
	jne	find_descriptor_2

end_list_2:
	mov	rdx,rbx 
	movzx	rbx,word ptr (-2)[rbx]
	cmp	rbx,256
	jb	no_record_arguments

	movzx	rdx,word ptr (-2+2)[rdx]
	sub	rdx,2
	jae	copy_record_arguments_aa

	sub	rbx,256+3

	mov	rdx,rax
	mov	rax,qword ptr [rax]
	jmp	begin_update_up_list_1r

update_up_list_1r:
	lea	rdx,(-3)[rax]
	mov	rax,qword ptr (-3)[rax]
begin_update_up_list_1r:
	mov	qword ptr [rdx],rdi
	test	rax,1
	jne	update_up_list_1r

copy_argument_part_1r:
	lea	rdx,(-2)[rax]

	mov	rax,qword ptr (-2)[rax]
	mov	qword ptr [rdx],rdi

	mov	qword ptr [rdi],rax
	add	rdi,8

copy_b_record_argument_part_arguments:
	mov	rax,qword ptr [rcx]
	add	rcx,8
	mov	qword ptr [rdi],rax 
	add	rdi,8
	sub	rbx,1
	jnc	copy_b_record_argument_part_arguments

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

copy_record_arguments_aa:
	sub	rbx,256+2
	sub	rbx,rdx 
	
	push	rbx 
	push	rdx 

update_up_list_2r:
	mov	rdx,rax 
	mov	rax,qword ptr [rdx]
	mov	rbx,3
	and	rbx,rax 
	sub	rbx,3
	jne	copy_argument_part_2r

	mov	qword ptr [rdx],rdi 
	sub	rax,3
	jmp	update_up_list_2r

copy_argument_part_2r:
	mov	qword ptr [rdx],rdi 
	cmp	rax,rcx 
	jb	copy_record_argument_2

	cmp	rax,qword ptr end_heap_p3+0
	jae	copy_record_argument_2

	mov	rdx,rax 
	mov	rax,qword ptr [rdx]
	lea	rbx,1[rdi]
	mov	qword ptr [rdx],rbx 
copy_record_argument_2:
	mov	qword ptr [rdi],rax 
	add	rdi,8

	pop	rbx 
	sub	rbx,1
	jc	no_pointers_in_record

copy_record_pointers:
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jb	copy_record_pointers_2

	cmp	rdx,qword ptr end_heap_p3+0
	jae	copy_record_pointers_2

	mov	rax,qword ptr [rdx]
	inc	rdi 
	mov	qword ptr [rdx],rdi 
	dec	rdi 
	mov	rdx,rax 
copy_record_pointers_2:
	mov	qword ptr [rdi],rdx 
	add	rdi,8
	sub	rbx,1
	jnc	copy_record_pointers

no_pointers_in_record:
	pop	rbx 
	
	sub	rbx,1
	jc	no_non_pointers_in_record

copy_non_pointers_in_record:
	mov	rax,qword ptr [rcx]
	add	rcx,8
	mov	qword ptr [rdi],rax 
	add	rdi,8
	sub	rbx,1
	jnc	copy_non_pointers_in_record

no_non_pointers_in_record:

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

no_record_arguments:
	sub	rbx,3
update_up_list_2:
	mov	rdx,rax
	mov	rax,qword ptr [rax]
	xor	rax,3
	mov	qword ptr [rdx],rdi
	test	al,3
	je	update_up_list_2

copy_argument_part_2:
	xor	rax,3
	cmp	rax,rcx 
	jc	copy_arguments_1

	cmp	rax,qword ptr end_heap_p3+0
	jnc	copy_arguments_1

	mov	rdx,rax
	mov	rax,qword ptr [rax]
	inc	rdi 
	mov	qword ptr [rdx],rdi 
	dec	rdi 
copy_arguments_1:
	mov	qword ptr [rdi],rax 
	add	rdi,8

copy_argument_part_arguments:
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jc	copy_arguments_2

	cmp	rdx,qword ptr end_heap_p3+0
	jnc	copy_arguments_2

	mov	rax,qword ptr [rdx]
	inc	rdi 
	mov	qword ptr [rdx],rdi 
	dec	rdi
	mov	rdx,rax 
copy_arguments_2:
	mov	qword ptr [rdi],rdx 
	add	rdi,8
	sub	rbx,1
	jnc	copy_argument_part_arguments

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

update_list_2_:
	mov	qword ptr [rdx],rdi 
begin_update_list_2:
	dec	rax
	mov	rdx,rax 
	mov	rax,qword ptr [rax]
update_list__2:
	test	rax,1
	jz	end_update_list_2
	test	rax,2
	jz	update_list_2_
	lea	rdx,(-3)[rax]
	mov	rax,qword ptr (-3)[rax]
	jmp	update_list__2

end_update_list_2:
	mov	qword ptr [rdx],rdi 

	mov	qword ptr [rdi],rax 
	add	rdi,8

	test	al,2
	je	move_lazy_node

	movzx	rbx,word ptr (-2)[rax]
	test	rbx,rbx 
	je	move_hnf_0

	cmp	rbx,256
	jae	move_record

	sub	rbx,2
	jc	move_hnf_1
	je	move_hnf_2

move_hnf_3:
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jc	move_hnf_3_1

	cmp	rdx,qword ptr end_heap_p3+0
	jnc	move_hnf_3_1

	lea	rax,1[rdi]
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rax 
	mov	rdx,rbx 
move_hnf_3_1:
	mov	qword ptr [rdi],rdx 
	
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jc	move_hnf_3_2

	lea	rax,(8+2+1)[rdi]
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rax 
	mov	rdx,rbx
move_hnf_3_2:
	mov	qword ptr 8[rdi],rdx 
	add	rdi,16

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_hnf_2:
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jc	move_hnf_2_1

	cmp	rdx,qword ptr end_heap_p3+0
	jnc	move_hnf_2_1

	lea	rax,1[rdi]
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rax 
	mov	rdx,rbx 
move_hnf_2_1:
	mov	qword ptr [rdi],rdx 
	
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jc	move_hnf_2_2

	cmp	rdx,qword ptr end_heap_p3+0
	jnc	move_hnf_2_2

	lea	rax,(8+1)[rdi]
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rax 
	mov	rdx,rbx
move_hnf_2_2:
	mov	qword ptr 8[rdi],rdx 
	add	rdi,16

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_hnf_1:
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jc	move_hnf_1_

	cmp	rdx,qword ptr end_heap_p3+0
	jnc	move_hnf_1_

	lea	rax,1[rdi]
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rax 
	mov	rdx,rbx
move_hnf_1_:
	mov	qword ptr [rdi],rdx 
	add	rdi,8

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_record:
	sub	rbx,258
	jb	move_record_1
	je	move_record_2

move_record_3:
	movzx	rbx,word ptr (-2+2)[rax]
	sub	rbx,1
	ja	move_hnf_3

	mov	rdx,qword ptr [rcx]
	lea	rcx,8[rcx]
	jb	move_record_3_1b

move_record_3_1a:
	cmp	rdx,rcx 
	jb	move_record_3_1b

	cmp	rdx,qword ptr end_heap_p3+0
	jae	move_record_3_1b

	lea	rax,1[rdi]
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rax 
	mov	rdx,rbx 
move_record_3_1b:
	mov	qword ptr [rdi],rdx 
	add	rdi,8

	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jb	move_record_3_2

	mov	rbx,qword ptr neg_heap_p3+0
	add	rbx,rdx

	mov	rax,rbx
	shr	rbx,6

	and	rax,31*8
	and	rbx,-4

	sub	rbx,qword ptr neg_heap_vector_plus_4+0
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr bit_set_table2[rax]
 endif

	cmp	rbx,r8
	jne	move_record_3_1b_bit_not_in_same_word

	mov	rbx,qword ptr [rdx]

	test	eax,esi
	jne	linked_record_argument_part_3_b_

	or	esi,eax
	jmp	not_linked_record_argument_part_3_b_

move_record_3_1b_bit_not_in_same_word:
	test	eax,dword ptr (-4)[rbx]
	je	not_linked_record_argument_part_3_b

linked_record_argument_part_3_b:
	mov	rbx,qword ptr [rdx]
linked_record_argument_part_3_b_:
	lea	rax,(2+1)[rdi]
	mov	qword ptr [rdx],rax
	mov	rdx,rbx
	jmp	move_record_3_2

not_linked_record_argument_part_3_b:
	or	dword ptr (-4)[rbx],eax

	mov	rbx,qword ptr [rdx]
not_linked_record_argument_part_3_b_:
	lea	rax,2[rdi]
	mov	qword ptr [rdx],rax
	mov	rdx,rbx

move_record_3_2:
	mov	qword ptr [rdi],rdx 
	add	rdi,8

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_record_2:
	cmp	word ptr (-2+2)[rax],1
	ja	move_hnf_2
	jb	move_record_2bb

move_record_2_ab:
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jb	move_record_2_1

	cmp	rdx,qword ptr end_heap_p3+0
	jae	move_record_2_1

	lea	rax,1[rdi]
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rax 
	mov	rdx,rbx
move_record_2_1:
	mov	qword ptr [rdi],rdx 
	mov	rbx,qword ptr [rcx]
	add	rcx,8
	mov	qword ptr 8[rdi],rbx 
	add	rdi,16

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_record_1:
	movzx	rbx,word ptr (-2+2)[rax]
	test	rbx,rbx 
	jne	move_hnf_1
	jmp	move_real_int_bool_or_char

move_record_2bb:
	mov	rax,qword ptr [rcx]
	add	rcx,8
	mov	qword ptr [rdi],rax 
	add	rdi,8
move_real_int_bool_or_char:
	mov	rax,qword ptr [rcx]
	add	rcx,8
	mov	qword ptr [rdi],rax 
	add	rdi,8
copy_normal_hnf_0:

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_hnf_0:
 ifdef PIC
	lea	r9,__STRING__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __STRING__+2
 endif
	jbe	move_string_or_array
 ifdef PIC
	lea	r9,CHAR+2+0
	cmp	rax,r9
 else
	cmp	rax,offset CHAR+2
 endif
	jbe	move_real_int_bool_or_char
 ifdef PIC
move_normal_hnf_0:
 endif

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_string_or_array:
	jne	move_array

	mov	rax,qword ptr [rcx]
	add	rax,7
	shr	rax,3

cp_s_arg_lp3:
	mov	rbx,qword ptr [rcx]
	add	rcx,8
	mov	qword ptr [rdi],rbx 
	add	rdi,8
st_cp_s_arg_lp3:
	sub	rax,1
	jnc	cp_s_arg_lp3

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_array:
 ifdef PIC
	lea	r9,__ARRAY__+2+0
	cmp	rax,r9
	jb	move_normal_hnf_0
 endif

	mov	rbx,qword ptr [rcx]
	mov	qword ptr [rdi],rbx

 ifdef PIC
	lea	r9,__ARRAY__R__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__R__+2
 endif
	ja	move_strict_basic_array
	jb	move_lazy_array

	mov	rdx,qword ptr 8[rcx]
	mov	qword ptr 8[rdi],rdx
	add	rcx,16
	add	rdi,16

	movzx	rax,word ptr (-2)[rdx]
	movzx	rdx,word ptr (-2+2)[rdx]
	sub	rax,256
	test	rdx,rdx
	je	move_b_record_array

	sub	rax,rdx
	je	move_a_record_array

	push	rdx
	push	rax
	push	rbx
	jmp	st_move_array_lp_ab

move_strict_basic_array:
	add	rcx,8
	add	rdi,8

 ifdef PIC
	lea	r9,__ARRAY__INT__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__INT__+2
 endif
	jbe	move_int_or_real_array

 ifdef PIC
	lea	r9,__ARRAY__BOOL__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__BOOL__+2
 endif
	ja	move_unboxed_basic_arrayp2
	je	move_bool_array

move_int32_or_real32_array:
	lea	rax,1[rbx]
	shr	rax,1
	jmp	st_cp_s_arg_lp3

move_int_or_real_array:
	mov	rax,rbx
	jmp	st_cp_s_arg_lp3

move_bool_array:
	lea	rax,7[rbx]
	shr	rax,3
	jmp	st_cp_s_arg_lp3

move_unboxed_basic_arrayp2:
 ifdef PIC
	lea	r9,__ARRAYP2__INT__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAYP2__INT__+2
 endif
	jbe	move_int_or_real_arrayp2

 ifdef PIC
	lea	r9,__ARRAYP2__BOOL__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAYP2__BOOL__+2
 endif
	jae	move_bool_or_char_arrayp2

move_int32_or_real32_arrayp2:
	cmp	rbx,2
	jbe	move_int32_or_real32_array

	lea	rdx,-1[rbx]
	bsr	rax,rdx
	xor	rdx,rdx
	bts	rdx,rax

	lea	rax,1[rbx]
	lea	rdx,[rdi+rdx*8]
	shr	rax,1
	jmp	st_cp_arrayp2_lp3

move_int_or_real_arrayp2:
	cmp	rbx,1
	jbe	move_int_or_real_arrayp2_01

	lea	rdx,-1[rbx]
	bsr	rax,rdx
	xor	rdx,rdx
	bts	rdx,rax

	add	rdx,rdx

	mov	rax,rbx
	lea	rdx,[rdi+rdx*8]
	jmp	st_cp_arrayp2_lp3

move_int_or_real_arrayp2_01:
	mov	rax,rbx
	add	rbx,rbx
	lea	rdx,[rdi+rbx*8]
	jmp	st_cp_arrayp2_lp3

move_bool_or_char_arrayp2:
	cmp	rbx,16
	jbe	move_bool_array

	lea	rdx,-1[rbx]
	bsr	rax,rdx
	xor	rdx,rdx
	bts	rdx,rax

	lea	rax,7[rbx]
	lea	rdx,[rdi+rdx*2]
	shr	rax,3
	jmp	st_cp_arrayp2_lp3

cp_arrayp2_lp3:
	mov	rbx,qword ptr [rcx]
	add	rcx,8
	mov	qword ptr [rdi],rbx
	add	rdi,8
st_cp_arrayp2_lp3:
	sub	rax,1
	jnc	cp_arrayp2_lp3

	mov	rdi,rdx

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_b_record_array:
	imul	rax,rbx
	jmp	st_cp_s_arg_lp3

move_a_record_array:
	imul	rbx,rdx
	jmp	st_move_array_lp

move_lazy_array:
	add	rcx,8
	add	rdi,8

 ifdef PIC
	lea	r9,__ARRAY__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__+2
 endif
	je	st_move_array_lp

move_lazy_arrayp2:
	mov	rax,rbx

	cmp	rbx,1
	jbe	move_lazy_arrayp2_01

	lea	rax,-1[rbx]
	bsr	rdx,rax
	xor	eax,eax
	bts	rax,rdx

move_lazy_arrayp2_01:
	shl	rax,1+3
	add	rax,rdi
	push	rax

	jmp	st_move_arrayp2_lp

move_array_ab_lp1:
	mov	rax,qword ptr 16[rsp]
move_array_ab_a_elements:
	mov	rbx,qword ptr [rcx]
	add	rcx,8
	cmp	rbx,rcx 
	jb	move_array_element_ab

	cmp	rbx,qword ptr end_heap_p3+0
	jnc	move_array_element_ab

	mov	rdx,rbx 
	mov	rbx,qword ptr [rbx]
	inc	rdi
	mov	qword ptr [rdx],rdi 
	dec	rdi
move_array_element_ab:
	mov	qword ptr [rdi],rbx 
	add	rdi,8
	sub	rax,1
	jne	move_array_ab_a_elements

	mov	rax,qword ptr 8[rsp]
move_array_ab_b_elements:
	mov	rbx,qword ptr [rcx]
	add	rcx,8
	mov	qword ptr [rdi],rbx 
	add	rdi,8
	sub	rax,1
	jne	move_array_ab_b_elements

st_move_array_lp_ab:
	sub	qword ptr [rsp],1
	jnc	move_array_ab_lp1

	add	rsp,24
	jmp	end_array	

move_array_lp1:
	mov	rax,qword ptr [rcx]
	add	rcx,8
	add	rdi,8
	cmp	rax,rcx 
	jb	move_array_element

	cmp	rax,qword ptr end_heap_p3+0
	jnc	move_array_element

	mov	rdx,qword ptr [rax]
	mov	qword ptr (-8)[rdi],rdx
	lea	rdx,(-8+1)[rdi]
	mov	qword ptr [rax],rdx

	sub	rbx,1
	jnc	move_array_lp1

	jmp	end_array

move_array_element:
	mov	qword ptr (-8)[rdi],rax 
st_move_array_lp:
	sub	rbx,1
	jnc	move_array_lp1

end_array:
	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_arrayp2_lp1:
	mov	rax,qword ptr [rcx]
	add	rcx,8
	add	rdi,8
	cmp	rax,rcx
	jb	move_arrayp2_element

	cmp	rax,qword ptr end_heap_p3+0
	jnc	move_arrayp2_element

	mov	rdx,qword ptr [rax]
	mov	qword ptr (-8)[rdi],rdx
	lea	rdx,(-8+1)[rdi]
	mov	qword ptr [rax],rdx

	sub	rbx,1
	jnc	move_arrayp2_lp1

	jmp	end_arrayp2

move_arrayp2_element:
	mov	qword ptr (-8)[rdi],rax
st_move_arrayp2_lp:
	sub	rbx,1
	jnc	move_arrayp2_lp1

end_arrayp2:
	pop	rdi

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_lazy_node:
	mov	rdx,rax 
	movsxd	rbx,dword ptr (-4)[rdx]
	test	rbx,rbx 
	je	move_lazy_node_0

	sub	rbx,1
ifdef PROFILE_GRAPH
	jle	move_selector_or_indirection
else
	jle	move_lazy_node_1
endif

	cmp	rbx,256
	jge	move_closure_with_unboxed_arguments

move_lazy_node_arguments:
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jc	move_lazy_node_arguments_

	cmp	rdx,qword ptr end_heap_p3+0
	jnc	move_lazy_node_arguments_

	mov	rax,qword ptr [rdx]
	mov	qword ptr [rdi],rax 
	lea	rax,1[rdi]
	add	rdi,8
	mov	qword ptr [rdx],rax 
	sub	rbx,1
	jnc	move_lazy_node_arguments

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_lazy_node_arguments_:
	mov	qword ptr [rdi],rdx 
	add	rdi,8
	sub	rbx,1
	jnc	move_lazy_node_arguments
	
	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

ifdef PROFILE_GRAPH
move_selector_or_indirection:
	mov	rbx,257
	cmp	rbx,0
	jmp	move_closure_with_unboxed_arguments
endif

move_lazy_node_1:
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jc	move_lazy_node_1_

	cmp	rdx,qword ptr end_heap_p3+0
	jnc	move_lazy_node_1_

	lea	rax,1[rdi]
	mov	rbx,qword ptr [rdx]
	mov	qword ptr [rdx],rax 
	mov	rdx,rbx 
move_lazy_node_1_:
	mov	qword ptr [rdi],rdx 
	add	rdi,16

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_lazy_node_0:
	add	rdi,16

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_closure_with_unboxed_arguments:
	je	move_closure_with_unboxed_arguments_1
	add	rbx,1
	mov	rax,rbx 
	and	rbx,255
	shr	rax,8
	sub	rbx,rax 
	je	move_non_pointers_of_closure

	push	rax 

move_closure_with_unboxed_arguments_lp:
	mov	rdx,qword ptr [rcx]
	add	rcx,8
	cmp	rdx,rcx 
	jc	move_closure_with_unboxed_arguments_

	cmp	rdx,qword ptr end_heap_p3+0
	jnc	move_closure_with_unboxed_arguments_

	mov	rax,qword ptr [rdx]
	mov	qword ptr [rdi],rax 
	lea	rax,1[rdi]
	add	rdi,8
	mov	qword ptr [rdx],rax 
	sub	rbx,1
	jne	move_closure_with_unboxed_arguments_lp

	pop	rax 
	jmp	move_non_pointers_of_closure

move_closure_with_unboxed_arguments_:
	mov	qword ptr [rdi],rdx 
	add	rdi,8
	sub	rbx,1
	jne	move_closure_with_unboxed_arguments_lp

	pop	rax 

move_non_pointers_of_closure:
	mov	rbx,qword ptr [rcx]
	add	rcx,8
	mov	qword ptr [rdi],rbx 
	add	rdi,8
	sub	rax,1
	jne	move_non_pointers_of_closure

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

move_closure_with_unboxed_arguments_1:
	mov	rax,qword ptr [rcx]
	mov	qword ptr [rdi],rax 
	add	rdi,16

	test	rsi,rsi
	jne	bsf_and_copy_nodes
	jmp	find_non_zero_long

end_move:

	mov	rcx,qword ptr finalizer_list+0

restore_finalizer_descriptors:
	lea	r9,__Nil-8+0
	cmp	rcx,r9
	je	end_restore_finalizer_descriptors

 ifdef PIC
	lea	r9,e____system__kFinalizer+2+0
	mov	qword ptr [rcx],r9
 else
	mov	qword ptr [rcx],offset e____system__kFinalizer+2
 endif
	mov	rcx,qword ptr 8[rcx]
	jmp	restore_finalizer_descriptors

end_restore_finalizer_descriptors:

 ifdef GC_HOOKS
	mov	rax,qword ptr gc_hook_after_compact+0
	test	rax,rax
	je	no_gc_hook_after_compact
	call	rax
no_gc_hook_after_compact:
 endif

