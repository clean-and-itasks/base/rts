
rmark_using_reversal:
	push	rsi
	push	rsi
	mov	rsi,3
	jmp	rmarkr_node

rmark_using_reversal_:
	sub	rcx,8
rmark_array_using_reversal:
	push	rbx
	push	rsi
	cmp	rcx,rbx
	ja	rmark_no_undo_reverse_1
	mov	qword ptr [rsi],rcx
	mov	qword ptr [rcx],rax
rmark_no_undo_reverse_1:
	mov	rsi,3
	jmp	rmarkr_arguments

rmark_record_array_using_reversal:
	push	rbx
	push	rsi
	cmp	rcx,rbx
	ja	rmark_no_undo_reverse_3
	mov	qword ptr [rsi],rcx
 ifdef PIC
	lea	r9,__ARRAY__R__+2+0
	mov	qword ptr [rcx],r9
 else
	mov	qword ptr [rcx],offset __ARRAY__R__+2
 endif
rmark_no_undo_reverse_3:
	mov	rsi,3
	jmp	rmarkr_arguments

rmarkr_hnf_2:
	or	qword ptr [rcx],2
	mov	rbp,qword ptr 8[rcx]
	mov	qword ptr 8[rcx],rsi
	lea	rsi,8[rcx]
	mov	rcx,rbp

rmarkr_node:
	mov	rax,qword ptr neg_heap_p3+0
	add	rax,rcx

	cmp	rax,qword ptr heap_size_64_65+0
	jnc	rmarkr_next_node_after_static

	mov	rbx,rax
	and	rax,31*8
	shr	rbx,8

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_set_table2)[rax]
 endif
	mov	ebp,dword ptr [rdi+rbx*4]

	test	rbp,rax
	jne	rmarkr_next_node

	or	rbp,rax
	mov	dword ptr [rdi+rbx*4],ebp

rmarkr_arguments:
	mov	rax,qword ptr [rcx]
 ifdef TEST_COMPACT_RMARKR
rmarkr_arguments_:
 endif
	test	al,2
	je	rmarkr_lazy_node

	movzx	rbp,word ptr (-2)[rax]
	test	rbp,rbp
	je	rmarkr_hnf_0

	add	rcx,8

	cmp	rbp,256
	jae	rmarkr_record

	sub	rbp,2
	je	rmarkr_hnf_2
	jc	rmarkr_hnf_1

rmarkr_hnf_3:
	mov	rdx,qword ptr 8[rcx]

	mov	rax,qword ptr neg_heap_p3+0
	add	rax,rdx

	mov	rbx,rax
	and	rax,31*8
	shr	rbx,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_set_table2)[rax]
 endif
	test	eax,[rdi+rbx*4]
	jne	rmarkr_shared_argument_part

	or	dword ptr [rdi+rbx*4],eax

rmarkr_no_shared_argument_part:
	or	qword ptr [rcx],2
	mov	qword ptr 8[rcx],rsi
	add	rcx,8

	or	qword ptr [rdx],1
	lea	rdx,[rdx+rbp*8]

	mov	rbp,qword ptr [rdx]
	mov	qword ptr [rdx],rcx
	mov	rsi,rdx
	mov	rcx,rbp
	jmp	rmarkr_node

rmarkr_shared_argument_part:
	cmp	rdx,rcx
	ja	rmarkr_hnf_1

	mov	rbx,qword ptr [rdx]
	lea	rax,(8+2+1)[rcx]
	mov	qword ptr [rdx],rax
	mov	qword ptr 8[rcx],rbx
	jmp	rmarkr_hnf_1

rmarkr_record:
	sub	rbp,258
	je	rmarkr_record_2
	jb	rmarkr_record_1

rmarkr_record_3:
	movzx	rbp,word ptr (-2+2)[rax]
	sub	rbp,1
	jb	rmarkr_record_3_bb
	je	rmarkr_record_3_ab
	dec	rbp
	je	rmarkr_record_3_aab
	jmp	rmarkr_hnf_3

rmarkr_record_3_bb:
	mov	rdx,qword ptr (16-8)[rcx]
	sub	rcx,8

	cmp	rdx,rcx
	ja	rmarkr_next_node

	mov	rax,qword ptr neg_heap_p3+0
	add	rax,rdx

	mov	rbp,rax
	and	rax,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_set_table2)[rax]
 endif
	test	eax,dword ptr [rdi+rbp*4]
	je	rmarkr_not_yet_linked_bb

	mov	rbp,qword ptr [rdx]
	lea	rax,(16+2+1)[rcx]
	mov	qword ptr 16[rcx],rbp 
	mov	qword ptr [rdx],rax 
	jmp	rmarkr_next_node

rmarkr_not_yet_linked_bb:
	or	dword ptr [rdi+rbp*4],eax

	mov	rbp,qword ptr [rdx]
	lea	rax,(16+2)[rcx]
	mov	qword ptr 16[rcx],rbp 
	mov	qword ptr [rdx],rax 
	jmp	rmarkr_next_node

rmarkr_record_3_ab:
	mov	rdx,qword ptr 8[rcx]

	cmp	rdx,rcx
	ja	rmarkr_hnf_1

	mov	rax,qword ptr neg_heap_p3+0
	add	rax,rdx

	mov	rbp,rax
	and	rax,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_set_table2)[rax]
 endif
	test	eax,dword ptr [rdi+rbp*4]
	je	rmarkr_not_yet_linked_ab

	mov	rbp,qword ptr [rdx]
	lea	rax,(8+2+1)[rcx]
	mov	qword ptr 8[rcx],rbp
	mov	qword ptr [rdx],rax
	jmp	rmarkr_hnf_1

rmarkr_not_yet_linked_ab:
	or	dword ptr [rdi+rbp*4],eax

	mov	rbp,qword ptr [rdx]
	lea	rax,(8+2)[rcx]
	mov	qword ptr 8[rcx],rbp 
	mov	qword ptr [rdx],rax
	jmp	rmarkr_hnf_1

rmarkr_record_3_aab:
	mov	rdx,qword ptr 8[rcx]

	mov	rax,qword ptr neg_heap_p3+0
	add	rax,rdx 

	mov	rbp,rax
	and	rax,31*8
	shr	rbp,8
 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_set_table2)[rax]
 endif
	test	eax,dword ptr [rdi+rbp*4]
	jne	rmarkr_shared_argument_part
	or	dword ptr [rdi+rbp*4],eax

	add	qword ptr [rcx],2
	mov	qword ptr 8[rcx],rsi
	add	rcx,8

	mov	rsi,qword ptr [rdx]
	mov	qword ptr [rdx],rcx 
	mov	rcx,rsi
	lea	rsi,1[rdx]
	jmp	rmarkr_node

rmarkr_record_2:
	cmp	word ptr (-2+2)[rax],1
	ja	rmarkr_hnf_2
	je	rmarkr_hnf_1
	sub	rcx,8
	jmp	rmarkr_next_node

rmarkr_record_1:
	cmp	word ptr (-2+2)[rax],0
	jne	rmarkr_hnf_1
	sub	rcx,8
	jmp	rmarkr_next_node

rmarkr_lazy_node_1:
	jne	rmarkr_selector_node_1

rmarkr_hnf_1:
	mov	rbp,qword ptr [rcx]
	mov	qword ptr [rcx],rsi 

	lea	rsi,2[rcx]
	mov	rcx,rbp 
	jmp	rmarkr_node

rmarkr_indirection_node:
	mov	rbx,qword ptr neg_heap_p3+0
	lea	rbx,(-8)[rcx+rbx]

	mov	rax,rbx
	and	rax,31*8
	shr	rbx,8
 ifdef PIC
	lea	r9,bit_clear_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_clear_table2)[rax]
 endif
	and	dword ptr [rdi+rbx*4],eax

	mov	rcx,qword ptr [rcx]
	jmp	rmarkr_node

rmarkr_selector_node_1:
	add	rbp,3
	je	rmarkr_indirection_node

	mov	rdx,qword ptr [rcx]

	mov	rbx,qword ptr neg_heap_p3+0
	add	rbx,rdx
	shr	rbx,3

	add	rbp,1
	jle	rmarkr_record_selector_node_1

	push	rax
	mov	rax,rbx

	shr	rbx,5
	and	rax,31

 ifdef PIC
	lea	r9,bit_set_table+0
	mov	eax,dword ptr [r9+rax*4]
 else
	mov	eax,dword ptr (bit_set_table)[rax*4]
 endif
	mov	ebx,dword ptr [rdi+rbx*4]
	and	rbx,rax 

	pop	rax
	jne	rmarkr_hnf_1

	mov	rbx,qword ptr [rdx]
	test	bl,2
	je	rmarkr_hnf_1

	cmp	word ptr (-2)[rbx],2
	jbe	rmarkr_small_tuple_or_record

rmarkr_large_tuple_or_record:
	mov	rbx,qword ptr 16[rdx]
	add	rbx,qword ptr neg_heap_p3+0
	shr	rbx,3

	push	rax
	mov	rax,rbx

	shr	rbx,5
	and	rax,31

 ifdef PIC
	lea	r9,bit_set_table+0
	mov	eax,dword ptr [r9+rax*4]
 else
	mov	eax,dword ptr (bit_set_table)[rax*4]
 endif
	mov	ebx,dword ptr [rdi+rbx*4]
	and	rbx,rax

	pop	rax
	jne	rmarkr_hnf_1

	mov	rbx,qword ptr neg_heap_p3+0
	lea	rbx,(-8)[rcx+rbx]

	push	rcx

 ifdef PIC
	movsxd	rcx,dword ptr (-8)[rax]
	add	rax,rcx
 else
	mov	eax,dword ptr (-8)[rax]
 endif

	mov	rcx,rbx
	and	rcx,31*8
	shr	rbx,8
 ifdef PIC
	lea	r9,bit_clear_table2+0
	mov	ecx,dword ptr [r9+rcx]
 else
	mov	ecx,dword ptr (bit_clear_table2)[rcx]
 endif
	and	dword ptr [rdi+rbx*4],ecx

 ifdef PIC
	movzx	eax,word ptr (4-8)[rax]
 else
	movzx	eax,word ptr 4[rax]
 endif
	cmp	rax,16
	jl	rmarkr_tuple_or_record_selector_node_2
	mov	rdx,qword ptr 16[rdx]
	je	rmarkr_tuple_selector_node_2
	mov	rcx,qword ptr (-24)[rdx+rax]
	pop	rdx
 ifdef PIC
	lea	r9,e__system__nind+0
	mov	qword ptr (-8)[rdx],r9
 else
	mov	qword ptr (-8)[rdx],offset e__system__nind
 endif
	mov	qword ptr [rdx],rcx
	jmp	rmarkr_node

rmarkr_tuple_selector_node_2:
	mov	rcx,qword ptr [rdx]
	pop	rdx
 ifdef PIC
	lea	r9,e__system__nind+0
	mov	qword ptr (-8)[rdx],r9
 else
	mov	qword ptr (-8)[rdx],offset e__system__nind
 endif
	mov	qword ptr [rdx],rcx
	jmp	rmarkr_node

rmarkr_record_selector_node_1:
	je	rmarkr_strict_record_selector_node_1

	push	rax
	mov	rax,rbx

	shr	rbx,5
	and	rax,31

 ifdef PIC
	lea	r9,bit_set_table+0
	mov	eax,dword ptr [r9+rax*4]
 else
	mov	eax,dword ptr (bit_set_table)[rax*4]
 endif
	mov	ebx,dword ptr [rdi+rbx*4]
	and	rbx,rax 

	pop	rax
	jne	rmarkr_hnf_1

	mov	rbx,qword ptr [rdx]
	test	bl,2
	je	rmarkr_hnf_1

	cmp	word ptr (-2)[rbx],258
	jbe	rmarkr_small_tuple_or_record

	mov	rbx,qword ptr 16[rdx]
	add	rbx,qword ptr neg_heap_p3+0
	shr	rbx,3

	push	rax
	mov	rax,rbx
	shr	rbx,5
	and	rax,31
 ifdef PIC
	lea	r9,bit_set_table+0
	mov	eax,dword ptr [r9+rax*4]
 else
	mov	eax,dword ptr (bit_set_table)[rax*4]
 endif
	mov	ebx,dword ptr [rdi+rbx*4]
	and	rbx,rax
	pop	rax
	jne	rmarkr_hnf_1

rmarkr_small_tuple_or_record:
	mov	rbx,qword ptr neg_heap_p3+0
	lea	rbx,(-8)[rcx+rbx]

	push	rcx

 ifdef PIC
	movsxd	rcx,dword ptr(-8)[rax]
	add	rax,rcx
 else
	mov	eax,(-8)[rax]
 endif

	mov	rcx,rbx
	and	rcx,31*8
	shr	rbx,8
 ifdef PIC
	lea	r9,bit_clear_table2+0
	mov	ecx,dword ptr [r9+rcx]
 else
	mov	ecx,dword ptr (bit_clear_table2)[rcx]
 endif
	and	dword ptr [rdi+rbx*4],ecx 

 ifdef PIC
	movzx	eax,word ptr (4-8)[rax]
 else
	movzx	eax,word ptr 4[rax]
 endif
	cmp	rax,16
	jle	rmarkr_tuple_or_record_selector_node_2
	mov	rdx,qword ptr 16[rdx]
	sub	rax,24
rmarkr_tuple_or_record_selector_node_2:
	mov	rcx,qword ptr [rdx+rax]
	pop	rdx
 ifdef PIC
	lea	r9,e__system__nind+0
	mov	qword ptr (-8)[rdx],r9
 else
	mov	qword ptr (-8)[rdx],offset e__system__nind
 endif
	mov	qword ptr [rdx],rcx
	jmp	rmarkr_node

rmarkr_strict_record_selector_node_1:
	push	rax
	mov	rax,rbx

	shr	rbx,5
	and	rax,31

 ifdef PIC
	lea	r9,bit_set_table+0
	mov	eax,dword ptr [r9+rax*4]
 else
	mov	eax,dword ptr (bit_set_table)[rax*4]
 endif
	mov	ebx,dword ptr [rdi+rbx*4]
	and	rbx,rax 

	pop	rax 
	jne	rmarkr_hnf_1

	mov	rbx,qword ptr [rdx]
	test	bl,2
	je	rmarkr_hnf_1

	cmp	word ptr (-2)[rbx],258
	jbe	rmarkr_select_from_small_record

	mov	rbx,qword ptr 16[rdx]
	add	rbx,qword ptr neg_heap_p3+0

	push	rax
	mov	rax,rbx

	shr	rbx,8
	and	rax,31*8

 ifdef PIC
	lea	r9,bit_set_table2+0
	mov	eax,dword ptr [r9+rax]
 else
	mov	eax,dword ptr (bit_set_table2)[rax]
 endif
	mov	ebx,dword ptr [rdi+rbx*4]
	and	rbx,rax

	pop	rax
	jne	rmarkr_hnf_1

rmarkr_select_from_small_record:
 ifdef PIC
	movsxd	rbx,dword ptr(-8)[rax]
	add	rax,rbx
 else
	mov	eax,(-8)[rax]
 endif
	sub	rcx,8

 ifdef PIC
	movzx	ebx,word ptr (4-8)[rax]
 else
	movzx	ebx,word ptr 4[rax]
 endif
	cmp	rbx,16
	jle	rmarkr_strict_record_selector_node_2
	add	rbx,qword ptr 16[rdx]
	mov	rbx,qword ptr (-24)[rbx]
	jmp	rmarkr_strict_record_selector_node_3
rmarkr_strict_record_selector_node_2:
	mov	rbx,qword ptr [rdx+rbx]
rmarkr_strict_record_selector_node_3:
	mov	qword ptr 8[rcx],rbx

 ifdef PIC
	movzx	ebx,word ptr (6-8)[rax]
 else
	movzx	ebx,word ptr 6[rax]
 endif
	test	rbx,rbx
	je	rmarkr_strict_record_selector_node_5
	cmp	rbx,16
	jle	rmarkr_strict_record_selector_node_4
	mov	rdx,qword ptr 16[rdx]
	sub	rbx,24
rmarkr_strict_record_selector_node_4:
	mov	rbx,qword ptr [rdx+rbx]
	mov	qword ptr 16[rcx],rbx
rmarkr_strict_record_selector_node_5:

 ifdef PIC
	mov	rax,qword ptr ((-8)-8)[rax]
 else
	mov	rax,qword ptr (-8)[rax]
 endif
	mov	qword ptr [rcx],rax
	jmp	rmarkr_next_node

; a2,d1: free

rmarkr_next_node:
	test	rsi,3
	jne	rmarkr_parent

	mov	rbp,qword ptr (-8)[rsi]
	mov	rbx,3
	
	and	rbx,rbp

	mov	rax,qword ptr [rsi]
	sub	rsi,8

	cmp	rbx,3
	je	rmarkr_argument_part_cycle1

	mov	qword ptr [rsi],rax

rmarkr_c_argument_part_cycle1:
	cmp	rcx,rsi 
	ja	rmarkr_no_reverse_1

	mov	rdx,qword ptr [rcx]
	lea	rax,(8+1)[rsi]
	mov	qword ptr 8[rsi],rdx
	mov	qword ptr [rcx],rax

	or	rsi,rbx
	mov	rcx,rbp
	xor	rcx,rbx
	jmp	rmarkr_node

rmarkr_no_reverse_1:
	mov	qword ptr 8[rsi],rcx
	mov	rcx,rbp
	or	rsi,rbx
	xor	rcx,rbx
	jmp	rmarkr_node

rmarkr_lazy_node:
	movsxd	rbp,dword ptr (-4)[rax]
	test	rbp,rbp
	je	rmarkr_next_node

	add	rcx,8

	sub	rbp,1
	jle	rmarkr_lazy_node_1

	cmp	rbp,255
	jge	rmarkr_closure_with_unboxed_arguments

rmarkr_closure_with_unboxed_arguments_:
	or	qword ptr [rcx],2
	lea	rcx,[rcx+rbp*8]

	mov	rbp,qword ptr [rcx]
	mov	qword ptr [rcx],rsi
	mov	rsi,rcx
	mov	rcx,rbp
	jmp	rmarkr_node

rmarkr_closure_with_unboxed_arguments:
; (a_size+b_size)+(b_size<<8)
;	add	rbp,1
	mov	rax,rbp
	and	rbp,255
	shr	rax,8
	sub	rbp,rax
;	sub	rbp,1
	jg	rmarkr_closure_with_unboxed_arguments_
	je	rmarkr_hnf_1
	sub	rcx,8
	jmp	rmarkr_next_node

rmarkr_hnf_0:
 ifdef PIC
	lea	r9,dINT+2+0
	cmp	rax,r9
 else
	cmp	rax,offset dINT+2
 endif
	je	rmarkr_int_3

 ifdef PIC
	lea	r9,CHAR+2+0
	cmp	rax,r9
 else
	cmp	rax,offset CHAR+2
 endif
 	je	rmarkr_char_3

	jb	rmarkr_no_normal_hnf_0

rmarkr_normal_hnf_0:
	mov	rbx,qword ptr neg_heap_p3+0
	add	rbx,rcx

	mov	rcx,rbx
	and	rcx,31*8
	shr	rbx,8
 ifdef PIC
	lea	r9,bit_clear_table2+0
	mov	ecx,dword ptr [r9+rcx]
 else
	mov	ecx,dword ptr (bit_clear_table2)[rcx]
 endif
	and	dword ptr [rdi+rbx*4],ecx 

	lea	rcx,((-8)-2)[rax]
	jmp	rmarkr_next_node_after_static

rmarkr_int_3:
	mov	rbp,qword ptr 8[rcx]
	cmp	rbp,33
	jnc	rmarkr_next_node

	mov	rbx,qword ptr neg_heap_p3+0
	add	rbx,rcx

	mov	rcx,rbx
	and	rcx,31*8
	shr	rbx,8
 ifdef PIC
	lea	r9,bit_clear_table2+0
	mov	ecx,dword ptr [r9+rcx]
 else
	mov	ecx,dword ptr (bit_clear_table2)[rcx]
 endif
	shl	rbp,4
	and	dword ptr [rdi+rbx*4],ecx 

 ifdef PIC
	lea	rcx,small_integers+0
	add	rcx,rbp
 else
	lea	rcx,(small_integers)[rbp]
 endif
	jmp	rmarkr_next_node_after_static

rmarkr_char_3:
	mov	rbx,qword ptr neg_heap_p3+0

	movzx	rax,byte ptr 8[rcx]
	add	rbx,rcx

	mov	rbp,rbx
	and	rbp,31*8
	shr	rbx,8
 ifdef PIC
	lea	r9,bit_clear_table2+0
	mov	ebp,dword ptr [r9+rbp]
 else
	mov	ebp,dword ptr (bit_clear_table2)[rbp]
 endif
	and	dword ptr [rdi+rbx*4],ebp

	shl	rax,4
 ifdef PIC
	lea	rcx,static_characters+0
	add	rcx,rax
 else
	lea	rcx,static_characters[rax]
 endif
	jmp	rmarkr_next_node_after_static

rmarkr_no_normal_hnf_0:
 ifdef PIC
	lea	r9,__ARRAY__+2+0
	cmp	rax,r9
	jb	rmarkr_normal_hnf_0
 endif

 ifdef PIC
	lea	r9,__ARRAY__R__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAY__R__+2
 endif
	ja	rmarkr_next_node
	jb	rmarkr_lazy_array

rmarkr_record_array:
	mov	rax,qword ptr 16[rcx]
	movzx	rbx,word ptr (-2)[rax]
	movzx	rax,word ptr (-2+2)[rax]
	test	rax,rax
	je	rmarkr_next_node

	sub	rbx,256
	cmp	rax,rbx
	je	rmarkr_a_record_array

rmarkr_ab_record_array:
	mov	rdx,qword ptr 8[rcx]
	test	rdx,rdx
	je	rmarkr_next_node

	imul	rdx,rbx
	sub	rdx,rbx

	mov	rbp,qword ptr 8[rcx]
	mov	rbx,rsi
	and	rsi,-4
	mov	qword ptr [rcx],rsi
	and	rbx,3
	shl	rbp,2
	or	rbp,rbx
	mov	qword ptr 8[rcx],rbp

	cmp	rax,1
	je	rmarkr_array_a_length_1

	add	rax,rdx
	lea	rsi,16[rcx+rax*8]

	mov	rbp,qword ptr [rsi]
	mov	rax,qword ptr 24[rcx+rdx*8]
	lea	rbx,3[rcx]
	mov	qword ptr 24[rcx+rdx*8],rbx
	mov	qword ptr [rsi],rax
	mov	rcx,rbp
	jmp	rmarkr_node

rmarkr_array_a_length_1:
	lea	rsi,24[rcx+rdx*8]

	mov	rbp,qword ptr [rsi]
	or	rcx,3
	mov	qword ptr [rsi],rcx

	or	rsi,3
	mov	rcx,rbp
	jmp	rmarkr_node

rmarkr_a_record_array:
	mov	rax,qword ptr 8[rcx]
	imul	rax,rbx

	test	rax,rax
	je	rmarkr_next_node

	mov	rbp,qword ptr 8[rcx]
	mov	rbx,rsi
	and	rsi,-4
	mov	qword ptr [rcx],rsi
	and	rbx,3
	shl	rbp,2
	or	rbp,rbx
	mov	qword ptr 8[rcx],rbp

	cmp	rax,1
	je	rmarkr_a_record_array_length_1

	lea	rsi,16[rcx+rax*8]
	mov	rdx,rcx 
	
	mov	rcx,qword ptr [rsi]
	mov	rax,qword ptr 24[rdx]
	lea	rbp,3[rdx]
	mov	qword ptr 24[rdx],rbp
	mov	qword ptr [rsi],rax
	jmp	rmarkr_node

rmarkr_a_record_array_length_1:
	mov	rdx,rcx

	mov	rcx,qword ptr 24[rcx]
	lea	rsi,3[rdx]
	mov	qword ptr 24[rdx],rsi

	lea	rsi,24+3[rdx]
	jmp	rmarkr_node

rmarkr_lazy_array:
	mov	rbp,qword ptr 8[rcx]
	test	rbp,rbp
	je	rmarkr_next_node

 ifdef PIC
	lea	r9,__ARRAYP2__+2+0
	cmp	rax,r9
 else
	cmp	rax,offset __ARRAYP2__+2
 endif
	sbb	rax,rax

	mov	rbx,rsi
	and	rsi,-4

	and	rax,2
	or	rax,rsi
	mov	qword ptr [rcx],rax

	mov	rax,rbp
	and	rbx,3
	shl	rbp,2
	or	rbp,rbx
	mov	qword ptr 8[rcx],rbp

	cmp	rax,1
	je	rmarkr_array_length_1

	lea	rsi,8[rcx+rax*8]
	mov	rdx,rcx

	mov	rcx,qword ptr [rsi]
	mov	rax,qword ptr 16[rdx]
	lea	rbp,3[rdx]
	mov	qword ptr 16[rdx],rbp
	mov	qword ptr [rsi],rax
	jmp	rmarkr_node

rmarkr_array_length_1:
	mov	rdx,rcx

	mov	rcx,qword ptr 16[rcx]
	lea	rsi,3[rdx]
	mov	qword ptr 16[rdx],rsi

	lea	rsi,16+3[rdx]
	jmp	rmarkr_node

; a2: free

rmarkr_parent:
	test	rsi,1
	jne	rmarkr_argument_part_parent_or_in_array_or_end

	mov	rbp,qword ptr -2[rsi]
	and	rsi,-4

	cmp	rcx,rsi
	ja	rmarkr_no_reverse_2

	mov	rdx,rcx
	lea	rax,1[rsi]
	mov	rcx,qword ptr [rdx]
	mov	qword ptr [rdx],rax

rmarkr_no_reverse_2:
	mov	qword ptr [rsi],rcx 
	lea	rcx,(-8)[rsi]
	mov	rsi,rbp
	jmp	rmarkr_next_node

rmarkr_argument_part_parent_or_in_array_or_end:
	test	rsi,2
	jne	rmarkr_in_array_or_end

rmarkr_argument_part_parent:
	mov	rbp,qword ptr -1[rsi]
	and	rsi,-4

	mov	rdx,rsi
	mov	rsi,rcx
	mov	rcx,rdx

rmarkr_skip_upward_pointers:
	mov	rax,rbp
	and	rax,3
	cmp	rax,3
	jne	rmarkr_no_upward_pointer

	lea	rdx,(-3)[rbp]
	mov	rbp,qword ptr (-3)[rbp]
	jmp	rmarkr_skip_upward_pointers

rmarkr_no_upward_pointer:
	cmp	rsi,rcx
	ja	rmarkr_no_reverse_3

	mov	rbx,rsi
	mov	rsi,qword ptr [rsi]
	lea	rax,1[rcx]
	mov	qword ptr [rbx],rax

rmarkr_no_reverse_3:
	mov	qword ptr [rdx],rsi
	lea	rsi,(-8)[rbp]

	and	rsi,-4

	mov	rdx,rsi
	mov	rbx,3

	mov	rbp,qword ptr [rsi]

	and	rbx,rbp
	mov	rax,qword ptr 8[rdx]

	or	rsi,rbx
	mov	qword ptr [rdx],rax

	cmp	rcx,rdx
	ja	rmarkr_no_reverse_4

	mov	rax,qword ptr [rcx]
	mov	qword ptr 8[rdx],rax
	lea	rax,(8+2+1)[rdx]
	mov	qword ptr [rcx],rax
	mov	rcx,rbp
	and	rcx,-4
	jmp	rmarkr_node

rmarkr_no_reverse_4:
	mov	qword ptr 8[rdx],rcx
	mov	rcx,rbp
	and	rcx,-4
	jmp	rmarkr_node

rmarkr_argument_part_cycle1:
	cmp	rbp,rsi
	jb	rmarkr_next_array_element

rmarkr_skip_pointer_list1:
	mov	rdx,rbp
	and	rdx,-4
	mov	rbp,qword ptr [rdx]
	mov	rbx,3
	and	rbx,rbp
	cmp	rbx,3
	je	rmarkr_skip_pointer_list1

	mov	qword ptr [rdx],rax
	jmp	rmarkr_c_argument_part_cycle1

rmarkr_next_array_element:
	cmp	rcx,rsi
	ja	rmarkr_no_reverse_5

	mov	rdx,qword ptr [rcx]
	lea	rbx,8+1[rsi]
	mov	qword ptr 8[rsi],rdx
	mov	qword ptr [rcx],rbx

	or	rsi,3
	mov	rcx,rax
	jmp	rmarkr_node

rmarkr_no_reverse_5:
	mov	qword ptr 8[rsi],rcx
	or	rsi,3
	mov	rcx,rax
	jmp	rmarkr_node

rmarkr_in_array_or_end:
	and	rsi,-4
	je	end_rmarkr

rmarkr_in_array:
	mov	rbp,qword ptr [rsi]

	cmp	rcx,rsi
	ja	rmarkr_no_reverse_6

	mov	rdx,rcx
	lea	rax,1[rsi]
	mov	rcx,qword ptr [rdx]
	mov	qword ptr [rdx],rax

rmarkr_no_reverse_6:
	mov	qword ptr [rsi],rcx

	lea	rbx,(3-24)[rsi]
	cmp	rbx,rbp
	ja	rmarkr_array_next_record
	je	rmarkr_array_last_record

	lea	rbp,(-16)[rsi]
	mov	rsi,qword ptr (-16)[rsi]
	mov	rcx,rbp

	mov	rax,qword ptr 8[rbp]
	mov	rbx,rax
	shr	rax,2
	and	rbx,3
	mov	qword ptr 8[rbp],rax
	jmp	rmarkr_end_array_

rmarkr_reversed_array_pointer:
	and	rsi,-4
	mov	rbp,rsi
	mov	rsi,qword ptr [rsi]
rmarkr_end_array_:
	test	rsi,1
	jne	rmarkr_reversed_array_pointer

	test	rsi,2
	jne	rmarkr_end_array__

 ifdef PIC
	lea	rax,__ARRAYP2__+2+0
 else
	mov	rax,offset __ARRAYP2__+2
 endif
	or	rsi,rbx
	mov	qword ptr [rbp],rax
	jmp	rmarkr_next_node

rmarkr_end_array__:
 ifdef PIC
	lea	rax,__ARRAY__+2+0
 else
	mov	rax,offset __ARRAY__+2
 endif
	and	rsi,-4
	or	rsi,rbx
	mov	qword ptr [rbp],rax
	jmp	rmarkr_next_node

rmarkr_array_last_record:
	lea	rbp,(-24)[rsi]
	mov	rsi,qword ptr (-24)[rsi]
	mov	rcx,rbp

	mov	rax,qword ptr 8[rbp]
	mov	rbx,rax
	shr	rax,2
	and	rbx,3
	mov	qword ptr 8[rbp],rax

 ifdef PIC
	lea	rax,__ARRAY__R__+2+0
 else
	mov	rax,offset __ARRAY__R__+2
 endif
	jmp	rmarkr_end_record_array_

rmarkr_reversed_record_array_pointer:
	and	rsi,-4
	mov	rbp,rsi
	mov	rsi,qword ptr [rsi]
rmarkr_end_record_array_:
	test	rsi,3
	jne	rmarkr_reversed_record_array_pointer

	or	rsi,rbx
	mov	qword ptr [rbp],rax
	jmp	rmarkr_next_node

rmarkr_array_next_record:
	mov	rbx,qword ptr (16-3)[rbp]

	movzx	rax,byte ptr (-2)[rbx]
	movzx	rbx,word ptr (-2+2)[rbx]

	cmp	rbx,1
	je	rmarkr_in_array_a1

	sub	rbx,rax
	neg	rax

	lea	rdx,[rsi+rax*8]
	lea	rsi,(-8)[rsi+rbx*8]

	mov	rcx,qword ptr [rsi]
	mov	rax,qword ptr [rdx]
	mov	qword ptr [rdx],rbp
	mov	qword ptr [rsi],rax
	jmp	rmarkr_node

rmarkr_in_array_a1:
	shl	rax,3
	sub	rsi,rax

	mov	rcx,qword ptr [rsi]
	mov	qword ptr [rsi],rbp
	or	rsi,3
	jmp	rmarkr_node

rmarkr_next_node_after_static:
	test	rsi,3
	jne	rmarkr_parent_after_static

	mov	rbp,qword ptr (-8)[rsi]
	mov	rbx,3
	
	and	rbx,rbp

	mov	rax,qword ptr [rsi]
	sub	rsi,8

	cmp	rbx,3
	je	rmarkr_argument_part_cycle2
	
	mov	qword ptr [rsi],rax

rmarkr_c_argument_part_cycle2:
	mov	qword ptr 8[rsi],rcx
	mov	rcx,rbp
	or	rsi,rbx
	xor	rcx,rbx
	jmp	rmarkr_node

rmarkr_parent_after_static:
	test	rsi,1
	jne	rmarkr_argument_part_parent_or_in_array_or_end_after_static

	mov	rbp,qword ptr -2[rsi]
	mov	qword ptr -2[rsi],rcx
	lea	rcx,(-10)[rsi]
	mov	rsi,rbp
	jmp	rmarkr_next_node

rmarkr_argument_part_parent_or_in_array_or_end_after_static:
	test	rsi,2
	jne	rmarkr_in_array_or_end_after_static

rmarkr_argument_part_parent_after_static:
	mov	rbp,qword ptr -1[rsi]
	and	rsi,-4

	mov	rdx,rsi
	mov	rsi,rcx
	mov	rcx,rdx

;	movl	rbp,qword ptr [rdx]
rmarkr_skip_upward_pointers_2:
	mov	rax,rbp
	and	rax,3
	cmp	rax,3
	jne	rmarkr_no_reverse_3

	lea	rdx,(-3)[rbp]
	mov	rbp,qword ptr (-3)[rbp]
	jmp	rmarkr_skip_upward_pointers_2

rmarkr_argument_part_cycle2:
	cmp	rbp,rsi
	jb	rmarkr_next_array_element_after_static

rmarkr_skip_pointer_list2:
	mov	rdx,rbp 
	and	rdx,-4
	mov	rbp,qword ptr [rdx]
	mov	rbx,3
	and	rbx,rbp
	cmp	rbx,3
	je	rmarkr_skip_pointer_list2

	mov	qword ptr [rdx],rax
	jmp	rmarkr_c_argument_part_cycle2

rmarkr_next_array_element_after_static:
	mov	qword ptr 8[rsi],rcx
	or	rsi,3
	mov	rcx,rax
	jmp	rmarkr_node

rmarkr_in_array_or_end_after_static:
	and	rsi,-4
	je	end_rmarkr_after_static

rmarkr_in_array_after_static:
	mov	rbp,qword ptr [rsi]
	jmp	rmarkr_no_reverse_6

end_rmarkr_after_static:
	mov	rsi,qword ptr [rsp]
	add	rsp,16
	mov	qword ptr [rsi],rcx
 ifdef TEST_COMPACT_RMARKR
	ret
 endif
	jmp	rmarkr_next_stack_node

end_rmarkr:
	pop	rsi
	pop	rbx

	cmp	rcx,rbx
	ja	rmark_no_reverse_4

	mov	rdx,rcx
	lea	rax,1[rsi]
	mov	rcx,qword ptr [rcx]
	mov	qword ptr [rdx],rax

rmark_no_reverse_4:
	mov	qword ptr [rsi],rcx
 ifdef TEST_COMPACT_RMARKR
	ret
 endif

rmarkr_next_stack_node:
	cmp	rsp,qword ptr end_stack+0
	jae	rmarkr_end

	mov	rcx,qword ptr [rsp]
	mov	rsi,qword ptr 8[rsp]
	add	rsp,16

	cmp	rcx,1
	ja	rmark_using_reversal

	jmp	rmark_next_node_

rmarkr_end:
	jmp	rmark_next_node
