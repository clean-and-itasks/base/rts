
rmark_using_reversal:
	pushl	a3
	pushl	a3
	movl	$3,a3
	jmp	rmarkr_node

rmark_using_reversal_:
	subl	$4,a0
rmark_array_using_reversal:
	pushl	d1
	pushl	a3
	cmpl	d1,a0
	ja	rmark_no_undo_reverse_1
	movl	a0,(a3)
	movl	d0,(a0)
rmark_no_undo_reverse_1:
	movl	$3,a3
	jmp	rmarkr_arguments

rmark_record_array_using_reversal:
	pushl	d1
	pushl	a3
	cmpl	d1,a0
	ja	rmark_no_undo_reverse_3
	movl	a0,(a3)
	movl	$__ARRAY__R__+2,(a0)
rmark_no_undo_reverse_3:
	movl	$3,a3
	jmp	rmarkr_arguments

rmarkr_hnf_2:
	orl	$2,(a0)
	movl	4(a0),a2
	movl	a3,4(a0)
	leal	4(a0),a3
	movl	a2,a0

rmarkr_node:
	movl	neg_heap_p3,d0
	addl	a0,d0
#ifdef SHARE_CHAR_INT
	cmpl	heap_size_32_33,d0
	jnc	rmarkr_next_node_after_static
#endif
	movl	d0,d1
	andl	$31*4,d0
	shrl	$7,d1
	movl	bit_set_table(d0),d0
	movl	(a4,d1,4),a2
	test	d0,a2
	jne	rmarkr_next_node
	
	orl	d0,a2
	movl	a2,(a4,d1,4)

rmarkr_arguments:
	movl	(a0),d0
#ifdef TEST_COMPACT_RMARKR
rmarkr_arguments_:
#endif
	testb	$2,d0b
	je	rmarkr_lazy_node

	movzwl	-2(d0),a2
	test	a2,a2
	je	rmarkr_hnf_0

	addl	$4,a0

	cmp	$256,a2
	jae	rmarkr_record

	subl	$2,a2
	je	rmarkr_hnf_2
	jc	rmarkr_hnf_1

rmarkr_hnf_3:
	movl	4(a0),a1

	movl	neg_heap_p3,d0
	addl	a1,d0
	movl	d0,d1
	andl	$31*4,d0
	shrl	$7,d1
	movl	bit_set_table(d0),d0		
	test	(a4,d1,4),d0
	jne	rmarkr_shared_argument_part

	orl	d0,(a4,d1,4)	

rmarkr_no_shared_argument_part:
	orl	$2,(a0)
	movl	a3,4(a0)
	addl	$4,a0

	orl	$1,(a1)
	leal	(a1,a2,4),a1

	movl	(a1),a2
	movl	a0,(a1)
	movl	a1,a3
	movl	a2,a0
	jmp	rmarkr_node

rmarkr_shared_argument_part:
	cmpl	a0,a1
	ja	rmarkr_hnf_1

	movl	(a1),d1
	leal	4+2+1(a0),d0
	movl	d0,(a1)
	movl	d1,4(a0)
	jmp	rmarkr_hnf_1

rmarkr_record:
	subl	$258,a2
	je	rmarkr_record_2
	jb	rmarkr_record_1

rmarkr_record_3:
	movzwl	-2+2(d0),a2
	subl	$1,a2
	jb	rmarkr_record_3_bb
	je	rmarkr_record_3_ab
	dec	a2
	je	rmarkr_record_3_aab
	jmp	rmarkr_hnf_3

rmarkr_record_3_bb:
	movl	8-4(a0),a1
	subl	$4,a0

	movl	neg_heap_p3,d0
	addl	a1,d0
	movl	d0,a2
	andl	$31*4,d0
	shrl	$7,a2
	movl	bit_set_table(d0),d0
	orl	d0,(a4,a2,4)

	cmpl	a0,a1
	ja	rmarkr_next_node

	add	d0,d0
	jne	rmarkr_bit_in_same_word1
	inc	a2
	mov	$1,d0
rmarkr_bit_in_same_word1:
	testl	(a4,a2,4),d0
	je	rmarkr_not_yet_linked_bb

	movl	neg_heap_p3,d0
	addl	a0,d0
	addl	$2*4,d0
	movl	d0,a2
	andl	$31*4,d0
	shrl	$7,a2
	movl	bit_set_table(d0),d0
	orl	d0,(a4,a2,4)

	movl	(a1),a2
	lea	8+2+1(a0),d0
	movl	a2,8(a0)
	movl	d0,(a1)
	jmp	rmarkr_next_node

rmarkr_not_yet_linked_bb:
	orl	d0,(a4,a2,4)
	movl	(a1),a2
	lea	8+2+1(a0),d0
	movl	a2,8(a0)
	movl	d0,(a1)
	jmp	rmarkr_next_node

rmarkr_record_3_ab:
	movl	4(a0),a1

	movl	neg_heap_p3,d0
	addl	a1,d0
	movl	d0,a2
	andl	$31*4,d0
	shrl	$7,a2
	movl	bit_set_table(d0),d0
	orl	d0,(a4,a2,4)

	cmpl	a0,a1
	ja	rmarkr_hnf_1

	add	d0,d0
	jne	rmarkr_bit_in_same_word2
	inc	a2
	mov	$1,d0
rmarkr_bit_in_same_word2:
	testl	(a4,a2,4),d0
	je	rmarkr_not_yet_linked_ab

	movl	neg_heap_p3,d0
	addl	a0,d0
	addl	$4,d0
	movl	d0,a2
	andl	$31*4,d0
	shrl	$7,a2
	movl	bit_set_table(d0),d0
	orl	d0,(a4,a2,4)

	movl	(a1),a2
	lea	4+2+1(a0),d0
	movl	a2,4(a0)
	movl	d0,(a1)
	jmp	rmarkr_hnf_1

rmarkr_not_yet_linked_ab: 
	orl	d0,(a4,a2,4)
	movl	(a1),a2
	lea	4+2+1(a0),d0
	movl	a2,4(a0)
	movl	d0,(a1)
	jmp	rmarkr_hnf_1

rmarkr_record_3_aab:
	movl	4(a0),a1

	movl	neg_heap_p3,d0
	addl	a1,d0
	movl	d0,a2
	andl	$31*4,d0
	shrl	$7,a2
	movl	bit_set_table(d0),d0
	testl	(a4,a2,4),d0
	jne	rmarkr_shared_argument_part
	orl	d0,(a4,a2,4)

	addl	$2,(a0)
	movl	a3,4(a0)
	addl	$4,a0
	
	movl	(a1),a3
	movl	a0,(a1)
	movl	a3,a0
	lea	1(a1),a3
	jmp	rmarkr_node

rmarkr_record_2:
	cmpw	$1,-2+2(d0)
	ja	rmarkr_hnf_2
	je	rmarkr_hnf_1
	subl	$4,a0
	jmp	rmarkr_next_node

rmarkr_record_1:
	cmpw	$0,-2+2(d0)
	jne	rmarkr_hnf_1
	subl	$4,a0
	jmp	rmarkr_next_node

rmarkr_lazy_node_1:
/ selectors:
	jne	rmarkr_selector_node_1

rmarkr_hnf_1:
	movl	(a0),a2
	movl	a3,(a0)

	leal	2(a0),a3
	movl	a2,a0
	jmp	rmarkr_node

/ selectors
rmarkr_indirection_node:
	movl	neg_heap_p3,d1
	leal	-4(a0,d1),d1

	movl	d1,d0
	andl	$31*4,d0
	shrl	$7,d1
	movl	bit_clear_table(d0),d0
	andl	d0,(a4,d1,4)

	movl	(a0),a0
	jmp	rmarkr_node

rmarkr_selector_node_1:
	addl	$3,a2
	je	rmarkr_indirection_node

	movl	(a0),a1

	movl	neg_heap_p3,d1
	addl	a1,d1
	shrl	$2,d1

	addl	$1,a2
	jle	rmarkr_record_selector_node_1

	push	d0
	movl	d1,d0
	shrl	$5,d1
	andl	$31,d0
	movl	bit_set_table(,d0,4),d0
	movl	(a4,d1,4),d1
	andl	d0,d1
	pop	d0
	jne	rmarkr_hnf_1

	movl	(a1),d1
	testb	$2,d1b
	je	rmarkr_hnf_1

	cmpw	$2,-2(d1)
	jbe	rmarkr_small_tuple_or_record

rmarkr_large_tuple_or_record:
	movl	8(a1),d1
	addl	neg_heap_p3,d1
	shrl	$2,d1

	push	d0
	movl	d1,d0
	shrl	$5,d1
	andl	$31,d0
	movl	bit_set_table(,d0,4),d0
	movl	(a4,d1,4),d1
	andl	d0,d1
	pop	d0
	jne	rmarkr_hnf_1

	movl	neg_heap_p3,d1
	lea	-4(a0,d1),d1

	push	a0

	movl	-8(d0),d0

	movl	d1,a0
	andl	$31*4,a0
	shrl	$7,d1
	movl	bit_clear_table(a0),a0
	andl	a0,(a4,d1,4)

	movzwl	4(d0),d0
	cmpl	$8,d0
	jl	rmarkr_tuple_or_record_selector_node_2
	movl	8(a1),a1
	je	rmarkr_tuple_selector_node_2
	movl	-12(a1,d0),a0
	pop	a1
	movl	$e__system__nind,-4(a1)
	movl	a0,(a1)
	jmp	rmarkr_node

rmarkr_tuple_selector_node_2:
	movl	(a1),a0
	pop	a1
	movl	$e__system__nind,-4(a1)
	movl	a0,(a1)
	jmp	rmarkr_node

rmarkr_record_selector_node_1:
	je	rmarkr_strict_record_selector_node_1

	push	d0
	movl	d1,d0
	shrl	$5,d1
	andl	$31,d0
	movl	bit_set_table(,d0,4),d0
	movl	(a4,d1,4),d1
	andl	d0,d1
	pop	d0
	jne	rmarkr_hnf_1

	movl	(a1),d1
	testb	$2,d1b
	je	rmarkr_hnf_1

	cmpw	$258,-2(d1)
	jbe	rmarkr_small_tuple_or_record

	movl	8(a1),d1
	addl	neg_heap_p3,d1
	shrl	$2,d1

	push	d0
	movl	d1,d0
	shrl	$5,d1
	andl	$31,d0
	movl	bit_set_table(,d0,4),d0
	movl	(a4,d1,4),d1
	andl	d0,d1
	pop	d0
	jne	rmarkr_hnf_1

rmarkr_small_tuple_or_record:
	movl	neg_heap_p3,d1
	lea	-4(a0,d1),d1

	push	a0

	movl	-8(d0),d0

	movl	d1,a0
	andl	$31*4,a0
	shrl	$7,d1
	movl	bit_clear_table(a0),a0
	andl	a0,(a4,d1,4)

	movzwl	4(d0),d0
	cmpl	$8,d0
	jle	rmarkr_tuple_or_record_selector_node_2
	movl	8(a1),a1
	subl	$12,d0
rmarkr_tuple_or_record_selector_node_2:
	movl	(a1,d0),a0
	pop	a1
	movl	$e__system__nind,-4(a1)
	movl	a0,(a1)
	jmp	rmarkr_node

rmarkr_strict_record_selector_node_1:
	push	d0
	movl	d1,d0
	shrl	$5,d1
	andl	$31,d0
	movl	bit_set_table(,d0,4),d0
	movl	(a4,d1,4),d1
	andl	d0,d1
	pop	d0
	jne	rmarkr_hnf_1

	movl	(a1),d1
	testb	$2,d1b
	je	rmarkr_hnf_1

	cmpw	$258,-2(d1)
	jbe	rmarkr_select_from_small_record

	movl	8(a1),d1
	addl	neg_heap_p3,d1

	push	d0
	movl	d1,d0
	shrl	$7,d1
	andl	$31*4,d0
	movl	bit_set_table(d0),d0
	movl	(a4,d1,4),d1
	andl	d0,d1
	pop	d0
	jne	rmarkr_hnf_1

rmarkr_select_from_small_record:
	movl	-8(d0),d0
	subl	$4,a0

	movzwl	4(d0),d1
	cmpl	$8,d1
	jle	rmarkr_strict_record_selector_node_2
	addl	8(a1),d1
	movl	-12(d1),d1
	jmp	rmarkr_strict_record_selector_node_3
rmarkr_strict_record_selector_node_2:
	movl	(a1,d1),d1
rmarkr_strict_record_selector_node_3:
	movl	d1,4(a0)

	movzwl	6(d0),d1
	testl	d1,d1
	je	rmarkr_strict_record_selector_node_5
	cmpl	$8,d1
	jle	rmarkr_strict_record_selector_node_4
	movl	8(a1),a1
	subl	$12,d1
rmarkr_strict_record_selector_node_4:
	movl	(a1,d1),d1
	movl	d1,8(a0)
rmarkr_strict_record_selector_node_5:

	movl	-4(d0),d0
	movl	d0,(a0)
	jmp	rmarkr_next_node

/ a2,d1: free

rmarkr_next_node:
	test	$3,a3
	jne	rmarkr_parent

	movl	-4(a3),a2
	movl	$3,d1
	
	andl	a2,d1

	movl	(a3),d0
	subl	$4,a3

	cmpl	$3,d1
	je	rmarkr_argument_part_cycle1

	movl	d0,(a3)

rmarkr_c_argument_part_cycle1:
	cmpl	a3,a0
	ja	rmarkr_no_reverse_1

	movl	(a0),a1
	leal	4+1(a3),d0
	movl	a1,4(a3)
	movl	d0,(a0)
	
	orl	d1,a3
	movl	a2,a0
	xorl	d1,a0
	jmp	rmarkr_node

rmarkr_no_reverse_1:
	movl	a0,4(a3)
	movl	a2,a0
	orl	d1,a3
	xorl	d1,a0
	jmp	rmarkr_node

rmarkr_lazy_node:
	movl	-4(d0),a2
	test	a2,a2
	je	rmarkr_next_node

	addl	$4,a0

	subl	$1,a2
	jle	rmarkr_lazy_node_1

	cmpl	$255,a2
	jge	rmarkr_closure_with_unboxed_arguments

rmarkr_closure_with_unboxed_arguments_:
	orl	$2,(a0)
	leal	(a0,a2,4),a0

	movl	(a0),a2
	movl	a3,(a0)
	movl	a0,a3
	movl	a2,a0
	jmp	rmarkr_node

rmarkr_closure_with_unboxed_arguments:
/ (a_size+b_size)+(b_size<<8)
/	addl	$1,a2
	movl	a2,d0
	andl	$255,a2
	shrl	$8,d0
	subl	d0,a2
/	subl	$1,a2
	jg	rmarkr_closure_with_unboxed_arguments_
	je	rmarkr_hnf_1
	subl	$4,a0
	jmp	rmarkr_next_node

rmarkr_hnf_0:
#ifdef SHARE_CHAR_INT
	cmpl	$INT+2,d0
	je	rmarkr_int_3

	cmpl	$CHAR+2,d0
 	je	rmarkr_char_3

	jb	rmarkr_no_normal_hnf_0

rmarkr_normal_hnf_0:
	movl	neg_heap_p3,d1
	addl	a0,d1
	movl	d1,a0
	andl	$31*4,a0
	shrl	$7,d1
	movl	bit_clear_table(a0),a0
	andl	a0,(a4,d1,4)

	lea	ZERO_ARITY_DESCRIPTOR_OFFSET-2(d0),a0
	jmp	rmarkr_next_node_after_static

rmarkr_int_3:
	movl	4(a0),a2
	cmpl	$33,a2
	jnc	rmarkr_next_node

	movl	neg_heap_p3,d1
	addl	a0,d1
	movl	d1,a0
	andl	$31*4,a0
	shrl	$7,d1
	movl	bit_clear_table(a0),a0
	andl	a0,(a4,d1,4)

	lea	small_integers(,a2,8),a0
	jmp	rmarkr_next_node_after_static

rmarkr_char_3:
	movl	neg_heap_p3,d1

	movzbl	4(a0),d0
	addl	a0,d1
	movl	d1,a2
	andl	$31*4,a2
	shrl	$7,d1
	movl	bit_clear_table(a2),a2
	andl	a2,(a4,d1,4)

	lea	static_characters(,d0,8),a0
	jmp	rmarkr_next_node_after_static
	
rmarkr_no_normal_hnf_0:
#endif
	cmpl	$__ARRAY__R__+2,d0
	ja	rmarkr_next_node
	jb	rmarkr_lazy_array

rmarkr_record_array:
	movl	8(a0),d0
	movzwl	-2(d0),d1
	movzwl	-2+2(d0),d0
	test	d0,d0
	je	rmarkr_next_node

	subl	$256,d1
	cmpl	d1,d0
	je	rmarkr_a_record_array

rmarkr_ab_record_array:
	movl	4(a0),a1
	test	a1,a1
	je	rmarkr_next_node

	imull	d1,a1
	subl	d1,a1

	movl	4(a0),a2
	movl	a3,d1
	andl	$-4,a3
	movl	a3,(a0)
	and	$3,d1
	shl	$2,a2
	orl	d1,a2
	movl	a2,4(a0)

	cmp	$1,d0
	je	rmarkr_array_a_length_1

	addl	a1,d0
	lea	8(a0,d0,4),a3

	movl	(a3),a2
	movl	12(a0,a1,4),d0
	lea	3(a0),d1
	movl	d1,12(a0,a1,4)
	movl	d0,(a3)
	movl	a2,a0
	jmp	rmarkr_node

rmarkr_array_a_length_1:
	lea	12(a0,a1,4),a3

	movl	(a3),a2
	orl	$3,a0
	movl	a0,(a3)

	orl	$3,a3
	movl	a2,a0
	jmp	rmarkr_node

rmarkr_a_record_array:
	movl	4(a0),d0
	imull	d1,d0

	testl	d0,d0
	je	rmarkr_next_node

	movl	4(a0),a2
	movl	a3,d1
	andl	$-4,a3
	movl	a3,(a0)
	and	$3,d1
	shl	$2,a2
	orl	d1,a2
	movl	a2,4(a0)

	cmpl	$1,d0
	je	rmarkr_a_record_array_length_1

	lea	8(a0,d0,4),a3
	movl	a0,a1

	movl	(a3),a0
	movl	12(a1),d0
	lea	3(a1),a2
	movl	a2,12(a1)
	movl	d0,(a3)
	jmp	rmarkr_node

rmarkr_a_record_array_length_1:
	movl	a0,a1

	movl	12(a0),a0
	lea	3(a1),a3
	movl	a3,12(a1)

	lea	12+3(a1),a3
	jmp	rmarkr_node

rmarkr_lazy_array:
	movl	4(a0),a2

	testl	a2,a2
	je	rmarkr_next_node

	cmpl	$__ARRAYP2__+2,d0
	sbb	d0,d0

	movl	a3,d1
	andl	$-4,a3

	and	$2,d0
	or	a3,d0
	movl	d0,(a0)

	movl	a2,d0
	and	$3,d1
	shl	$2,a2
	orl	d1,a2
	movl	a2,4(a0)

	cmpl	$1,d0
	je	rmarkr_array_length_1

	lea	4(a0,d0,4),a3
	movl	a0,a1

	movl	(a3),a0
	movl	8(a1),d0
	lea	3(a1),a2
	movl	a2,8(a1)
	movl	d0,(a3)
	jmp	rmarkr_node

rmarkr_array_length_1:
	movl	a0,a1

	movl	8(a0),a0
	lea	3(a1),a3
	movl	a3,8(a1)

	lea	8+3(a1),a3
	jmp	rmarkr_node

/ a2: free

rmarkr_parent:
	test	$1,a3
	jne	rmarkr_argument_part_parent_or_in_array_or_end

	movl	-2(a3),a2
	andl	$-4,a3
	
	cmpl	a3,a0
	ja	rmarkr_no_reverse_2

	movl	a0,a1
	leal	1(a3),d0
	movl	(a1),a0
	movl	d0,(a1)

rmarkr_no_reverse_2:
	movl	a0,(a3)
	leal	-4(a3),a0
	movl	a2,a3
	jmp	rmarkr_next_node
	
rmarkr_argument_part_parent_or_in_array_or_end:
	test	$2,a3
	jne	rmarkr_in_array_or_end

rmarkr_argument_part_parent:
	movl	-1(a3),a2
	andl	$-4,a3

	movl	a3,a1
	movl	a0,a3
	movl	a1,a0

rmarkr_skip_upward_pointers:
	movl	a2,d0
	andl	$3,d0
	cmpl	$3,d0
	jne	rmarkr_no_upward_pointer

	leal	-3(a2),a1
	movl	-3(a2),a2
	jmp	rmarkr_skip_upward_pointers

rmarkr_no_upward_pointer:
	cmpl	a0,a3
	ja	rmarkr_no_reverse_3

	movl	a3,d1
	movl	(a3),a3
	leal	1(a0),d0
	movl	d0,(d1)
	
rmarkr_no_reverse_3:
	movl	a3,(a1)
	lea	-4(a2),a3

	andl	$-4,a3

	movl	a3,a1
	movl	$3,d1

	movl	(a3),a2

	andl	a2,d1
	movl	4(a1),d0

	orl	d1,a3
	movl	d0,(a1)

	cmpl	a1,a0
	ja	rmarkr_no_reverse_4

	movl	(a0),d0
	movl	d0,4(a1)
	leal	4+2+1(a1),d0
	movl	d0,(a0)
	movl	a2,a0
	andl	$-4,a0
	jmp	rmarkr_node

rmarkr_no_reverse_4:
	movl	a0,4(a1)
	movl	a2,a0
	andl	$-4,a0
	jmp	rmarkr_node

rmarkr_argument_part_cycle1:
	cmpl	a3,a2
	jb	rmarkr_next_array_element

rmarkr_skip_pointer_list1:
	movl	a2,a1
	andl	$-4,a1
	movl	(a1),a2
	movl	$3,d1
	andl	a2,d1
	cmpl	$3,d1
	je	rmarkr_skip_pointer_list1

	movl	d0,(a1)
	jmp	rmarkr_c_argument_part_cycle1

rmarkr_next_array_element:
	cmpl	a3,a0
	ja	rmarkr_no_reverse_5

	movl	(a0),a1
	leal	4+1(a3),d1
	movl	a1,4(a3)
	movl	d1,(a0)

	orl	$3,a3
	movl	d0,a0
	jmp	rmarkr_node

rmarkr_no_reverse_5:
	movl	a0,4(a3)
	orl	$3,a3
	movl	d0,a0
	jmp	rmarkr_node

rmarkr_in_array_or_end:
	andl	$-4,a3
	je	end_rmarkr

rmarkr_in_array:
	movl	(a3),a2

	cmpl	a3,a0
	ja	rmarkr_no_reverse_6

	movl	a0,a1
	leal	1(a3),d0
	movl	(a1),a0
	movl	d0,(a1)

rmarkr_no_reverse_6:
	movl	a0,(a3)

	lea	(3-12)(a3),d1
	cmpl	a2,d1
	ja	rmarkr_array_next_record
	je	rmarkr_array_last_record

	lea	-8(a3),a2
	movl	-8(a3),a3
	movl	a2,a0

	movl	4(a2),d0
	movl	d0,d1
	shr	$2,d0
	andl	$3,d1
	movl	d0,4(a2)
	jmp	rmarkr_end_array_

rmarkr_reversed_array_pointer:
	andl	$-4,a3
	movl	a3,a2
	movl	(a3),a3
rmarkr_end_array_:
	testl	$1,a3
	jne	rmarkr_reversed_array_pointer

	testl	$2,a3
	jne	rmarkr_end_array__

	movl	$__ARRAYP2__+2,d0
	orl	d1,a3
	movl	d0,(a2)
	jmp	rmarkr_next_node

rmarkr_end_array__:
	movl	$__ARRAY__+2,d0
	orl	d1,a3
	movl	d0,(a2)
	jmp	rmarkr_next_node

rmarkr_array_last_record:
	lea	-12(a3),a2
	movl	-12(a3),a3
	movl	a2,a0

	movl	4(a2),d0
	movl	d0,d1
	shr	$2,d0
	andl	$3,d1
	movl	d0,4(a2)

	movl	$__ARRAY__R__+2,d0
	jmp	rmarkr_end_record_array_

rmarkr_reversed_record_array_pointer:
	andl	$-4,a3
	movl	a3,a2
	movl	(a3),a3
rmarkr_end_record_array_:
	testl	$3,a3
	jne	rmarkr_reversed_record_array_pointer

	orl	d1,a3
	movl	d0,(a2)
	jmp	rmarkr_next_node

rmarkr_array_next_record:
	movl	8-3(a2),d1

	movzbl	-2(d1),d0
	movzwl	-2+2(d1),d1

	cmp	$1,d1
	je	rmarkr_in_array_a1

	sub	d0,d1
	neg	d0

	lea	(a3,d0,4),a1
	lea	-4(a3,d1,4),a3

	movl	(a3),a0
	movl	(a1),d0
	movl	a2,(a1)
	movl	d0,(a3)
	jmp	rmarkr_node

rmarkr_in_array_a1:
	shl	$2,d0
	sub	d0,a3

	movl	(a3),a0
	movl	a2,(a3)
	orl	$3,a3
	jmp	rmarkr_node

#ifdef SHARE_CHAR_INT
rmarkr_next_node_after_static:
	test	$3,a3
	jne	rmarkr_parent_after_static

	movl	-4(a3),a2
	movl	$3,d1
	
	andl	a2,d1

	movl	(a3),d0
	subl	$4,a3

	cmpl	$3,d1
	je	rmarkr_argument_part_cycle2
	
	movl	d0,(a3)

rmarkr_c_argument_part_cycle2:
	movl	a0,4(a3)
	movl	a2,a0
	orl	d1,a3
	xorl	d1,a0
	jmp	rmarkr_node

rmarkr_parent_after_static:
	test	$1,a3
	jne	rmarkr_argument_part_parent_or_in_array_or_end_after_static

	movl	-2(a3),a2
	movl	a0,-2(a3)
	leal	-6(a3),a0
	movl	a2,a3
	jmp	rmarkr_next_node

rmarkr_argument_part_parent_or_in_array_or_end_after_static:
	test	$2,a3
	jne	rmarkr_in_array_or_end_after_static
	
rmarkr_argument_part_parent_after_static:
	movl	-1(a3),a2
	andl	$-4,a3

	movl	a3,a1
	movl	a0,a3
	movl	a1,a0

/	movl	(a1),a2
rmarkr_skip_upward_pointers_2:
	movl	a2,d0
	andl	$3,d0
	cmpl	$3,d0
	jne	rmarkr_no_reverse_3

/	movl	a2,a1
/	andl	$-4,a1
/	movl	(a1),a2
	lea	-3(a2),a1
	movl	-3(a2),a2
	jmp	rmarkr_skip_upward_pointers_2

rmarkr_argument_part_cycle2:
	cmpl	a3,a2
	jb	rmarkr_next_array_element_after_static

rmarkr_skip_pointer_list2:
	movl	a2,a1
	andl	$-4,a1
	movl	(a1),a2
	movl	$3,d1
	andl	a2,d1
	cmpl	$3,d1
	je	rmarkr_skip_pointer_list2

	movl	d0,(a1)
	jmp	rmarkr_c_argument_part_cycle2

rmarkr_next_array_element_after_static:
	movl	a0,4(a3)
	orl	$3,a3
	movl	d0,a0
	jmp	rmarkr_node

rmarkr_in_array_or_end_after_static:
	andl	$-4,a3
	je	end_rmarkr_after_static

rmarkr_in_array_after_static:
	movl	(a3),a2
	jmp	rmarkr_no_reverse_6
#endif

end_rmarkr_after_static:
	movl	(sp),a3
	addl	$8,sp
	movl	a0,(a3)
#ifdef TEST_COMPACT_RMARKR
	ret
#endif
	jmp	rmarkr_next_stack_node

end_rmarkr:
	popl	a3
	popl	d1

	cmpl	d1,a0
	ja	rmark_no_reverse_4

	movl	a0,a1
	leal	1(a3),d0
	movl	(a0),a0
	movl	d0,(a1)

rmark_no_reverse_4:
	movl	a0,(a3)
#ifdef TEST_COMPACT_RMARKR
	ret
#endif

rmarkr_next_stack_node:
	cmpl	end_stack,sp
	jae	rmark_next_node

	movl	(sp),a0
	movl	4(sp),a3
	addl	$8,sp
	
	cmpl	$1,a0
	ja	rmark_using_reversal

	jmp	rmark_next_node_
