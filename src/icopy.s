
#define COPY_RECORDS_WITHOUT_POINTERS_TO_END_OF_HEAP

	.equ	ws,4
	.equ	wl,2

	push	a3

	mov	heap_p1,a4
	negl	a4
	mov	a4,neg_heap_p1

	mov	heap_p2,a4

	mov	heap_size_129,d0
	shl	$6,d0

	mov	d0,semi_space_size
	lea	(a4,d0),a3

#ifdef WRITE_HEAP
	movl	a3,heap2_begin_and_end+4
#endif

	movl	caf_list,d0
	test	d0,d0
	je	end_copy_cafs

copy_cafs_lp:
	pushl	-ws(d0)
	
	lea	ws(d0),a2
	movl	(d0),d1
	subl	$1,d1
	call	copy_lp2
	
	popl	d0
	test	d0,d0
	jne	copy_cafs_lp

end_copy_cafs:
	movl	(sp),d1
	mov	stack_p,a2
	sub	a2,d1
	shr	$wl,d1

	sub	$1,d1
	jb	end_copy0
	call	copy_lp2
end_copy0:
	mov	heap_p2,a2

	jmp	copy_lp1
/
/	Copy all referenced nodes to the other semi space
/

in_hnf_1_2:
	dec	d1
copy_lp2_lp1:
	call	copy_lp2
copy_lp1:
	cmp	a4,a2
	jae	end_copy1

	mov	(a2),d0
	add	$ws,a2
	testb	$2,d0b
	je	not_in_hnf_1
in_hnf_1:
	movzwl	-2(d0),d1

	test	d1,d1
	je	copy_array_21

	cmp	$2,d1
	jbe	in_hnf_1_2

	cmp	$256,d1
	jae	copy_record_21

	push	d1
	xorl	d1,d1
	
	call	copy_lp2

	pop	d1
	add	$ws,a2

	sub	$2,d1
	jmp	copy_lp2_lp1

copy_record_21:
	subl	$258,d1
	ja	copy_record_arguments1_3

	movzwl	-2+2(d0),d1
	subl	$1,d1
	jmp	copy_lp2_lp1

copy_record_arguments1_3:
	movzwl	-2+2(d0),a1
	subl	$1,a1

	lea	3*ws(a2,d1,4),a0
	pushl	a0
	pushl	a1

	sub	d1,d1
	call	copy_lp2
	
	addl	$ws,a2
	popl	d1
	dec	d1
	call	copy_lp2

	popl	a2
	jmp	copy_lp1

not_in_hnf_1:
	mov	-4(d0),d1
	cmpl	$257,d1
	jge	copy_unboxed_closure_arguments
	sub	$1,d1
	jmp	copy_lp2_lp1

copy_unboxed_closure_arguments:
	mov	d1,d0
	shr	$8,d0
	movzbl	d1b,d1
	sub	d0,d1

	sub	$1,d1
	
	pushl	d0
	call	copy_lp2
	popl	d0

	lea	(a2,d0,4),a2
	jmp	copy_lp1

copy_array_21:
	cmpl	$__ARRAYP2__+2,d0
	jb	copy_array_21_a
	je	copy_arrayp2_21_a

	movl	ws(a2),d1
	addl	$2*ws,a2

	movzwl	-2(d1),d0
	movzwl	-2+2(d1),d1
	subl	$256,d0
	cmpl	d0,d1
	je	copy_array_21_r_a

copy_array_21_ab:
	cmpl	$0,-2*ws(a2)
	je	copy_lp1

	subl	d1,d0
	shl	$wl,d0
	subl	$1,d1

	pushl	d1
	pushl	d0
	movl	-2*ws(a2),d1
	subl	$1,d1
	pushl	d1

copy_array_21_lp_ab:
	movl	2*ws(sp),d1
	call	copy_lp2

	addl	ws(sp),a2
	subl	$1,(sp)
	jnc	copy_array_21_lp_ab
	
	addl	$3*ws,sp
	jmp	copy_lp1

copy_array_21_r_a:
	movl	-2*ws(a2),d1
	imull	d0,d1
	subl	$1,d1
	jc	copy_lp1
	jmp	copy_lp2_lp1

copy_array_21_a:
	movl	(a2),d1
	addl	$ws,a2
	subl	$1,d1
	jc	copy_lp1
	jmp	copy_lp2_lp1

copy_arrayp2_21_a:
	movl	(a2),d1
	addl	$ws,a2
	subl	$1,d1
	jc	copy_lp1
	je	copy_arrayp2_21_a_1

	bsr	d1,a0
	mov	$2*ws,d0
	shl	a0b,d0

	add	a2,d0
	push	d0
	call	copy_lp2
	pop	a2
	jmp	copy_lp1

copy_arrayp2_21_a_1:
	call	copy_lp2
	addl	$ws,a2
	jmp	copy_lp1

/
/	Copy nodes to the other semi-space
/

copy_lp2:
	movl	(a2),a1

/ selectors:
continue_after_selector_2:
	movl	(a1),a0
	testb	$2,a0b
	je	not_in_hnf_2

in_hnf_2:
	movzwl	-2(a0),d0
	test	d0,d0
	je	copy_arity_0_node2

	cmp	$256,d0
	jae	copy_record_2

	sub	$2,d0
	jb	copy_hnf_node2_1
	ja	copy_hnf_node2_3

copy_hnf_node2_2:
	mov	a4,(a2)
	lea	ws(a2),a2

copy_hnf_node2_2_:
	mov	a0,(a4)
	inc	a4
	mov	ws(a1),a0

	mov	a4,(a1)
	mov	2*ws(a1),d0

	sub	$1,d1
	mov	a0,ws-1(a4)

	mov	d0,2*ws-1(a4)
	lea	3*ws-1(a4),a4

	jae	copy_lp2
	ret

copy_hnf_node2_1:
	lea	-2*ws+1(a3),d0
	sub	$2*ws,a3
	mov	a3,(a2)
	add	$ws,a2
	mov	d0,(a1)
	mov	a0,(a3)
	mov	ws(a1),a1
	mov	a1,ws(a3)
	jmp	copy_lp3_

copy_hnf_node2_3:
	push	d1

	mov	2*ws(a1),d1

	testl	$1,(d1)
	jne	arguments_already_copied_2

	mov	a4,(a2)
	add	$ws,a2

copy_hnf_node2_3_:
	mov	a0,(a4)
	mov	d1,a0
	lea	1(a4),d1
	mov	d1,(a1)

	pop	d1

	mov	ws(a1),a1
	mov	a1,ws(a4)

	add	$3*ws,a4
	mov	(a0),a1

	mov	a4,-ws(a4)
	add	$ws,a0

	mov	a1,(a4)
	inc	a4

	mov	a4,-ws(a0)
	add	$ws-1,a4

cp_hnf_arg_lp2:
	mov	(a0),a1
	add	$ws,a0
	mov	a1,(a4)
	add	$ws,a4
	dec	d0
	jne	cp_hnf_arg_lp2

	sub	$1,d1
	jae	copy_lp2
	ret

arguments_already_copied_2:
	mov	a0,-3*ws(a3)
	mov	(d1),a0

	pop	d1

arguments_already_copied_2_:
	lea	-3*ws+1(a3),d0
	sub	$3*ws,a3
	mov	a3,(a2)
	add	$ws,a2
	mov	d0,(a1)
	mov	ws(a1),a1
	sub	$1,a0
	mov	a1,ws(a3)
	mov	a0,2*ws(a3)
	jmp	copy_lp3_
	
copy_arity_0_node2:
	cmp	$INT+2,a0
	jb	copy_real_file_or_string_2

	cmp	$CHAR+2,a0
	ja	copy_normal_hnf_0_2

copy_int_bool_or_char_2:
	mov	ws(a1),d0
	je	copy_char_2

	cmp	$INT+2,a0
	jne	no_small_int_or_char_2

copy_int_2:
	cmp	$33,d0
	jae	no_small_int_or_char_2
	shl	$wl+1,d0
	add	$ws,a2
	add	$small_integers,d0
	sub	$1,d1
	mov	d0,-ws(a2)
	jae	copy_lp2
	ret

copy_char_2:
	movzbl	d0b,d0
	shl	$wl+1,d0
	add	$ws,a2
	add	$static_characters,d0
	sub	$1,d1
	mov	d0,-ws(a2)
	jae	copy_lp2
	ret

no_small_int_or_char_2:

copy_record_node2_1_b:
	mov	a0,-2*ws(a3)
	add	$ws,a2

	mov	d0,-ws(a3)
	sub	$2*ws-1,a3

	mov	a3,(a1)
	dec	a3
	
	mov	a3,-ws(a2)

	sub	$1,d1
	jae	copy_lp2
	ret

copy_normal_hnf_0_2:
	sub	$2-ZERO_ARITY_DESCRIPTOR_OFFSET,a0
	sub	$1,d1

	mov	a0,(a2)
	lea	ws(a2),a2
	jae	copy_lp2
	ret

copy_real_file_or_string_2:
	cmpl	$__STRING__+2,a0
	jbe	copy_string_or_array_2

copy_real_or_file_2:
	mov	a0,-3*ws(a3)
	sub	$3*ws-1,a3

	mov	a3,(a1)
	dec	a3

	mov	ws(a1),d0
	mov	2*ws(a1),a0

	mov	a3,(a2)
	add	$ws,a2

	mov	d0,ws(a3)
	sub	$1,d1

	mov	a0,2*ws(a3)

	jae	copy_lp2
	ret

already_copied_2:
	dec	a0
	sub	$1,d1

	mov	a0,(a2)
	lea	ws(a2),a2
	
	jae	copy_lp2
	ret

copy_record_2:
	subl	$258,d0
	ja	copy_record_node2_3
	jb	copy_record_node2_1

	cmpw	$1,-2+2(a0)
	jb	copy_real_or_file_2
	ja	copy_hnf_node2_2

copy_record_node2_2ab:
	lea	-3*ws+1(a3),d0
	mov	a0,-3*ws(a3)
	sub	$3*ws,a3
	mov	2*ws(a1),a0
	mov	d0,(a1)
	mov	ws(a1),a1
	mov	a3,(a2)
	add	$ws,a2
	mov	a0,2*ws(a3)
	mov	a1,ws(a3)
	jmp	copy_lp3_

copy_record_node2_1:
	movl	ws(a1),d0

	cmpw	$0,-2+2(a0)
	je	copy_record_node2_1_b

copy_record_node2_1_a:
	mov	a0,-2*ws(a3)
	lea	-2*ws+1(a3),a0
	sub	$2*ws,a3
	mov	a0,(a1)
	mov	d0,a1
	mov	a3,(a2)
	add	$ws,a2
	mov	a1,ws(a3)
	jmp	copy_lp3_

copy_record_node2_3:
	cmpw	$1,-2+2(a0)
	ja	copy_hnf_node2_3
	jb	copy_record_node2_3_b

copy_record_node2_3_ab:
	push	a4
	push	d1

	movl	2*ws(a1),d1
	subl	heap_p1,d1
	mov	d1,a4
	shr	$wl+1,d1
	shr	$wl+4,a4
	and	$31,d1
	andl	$-4,a4
	mov	bit_set_table(,d1,4),d1
	addl	heap_copied_vector,a4
	test	(a4),d1
	jne	record_arguments_already_copied2_3_ab

	or	d1,(a4)

	pop	d1

	sub	$3*ws+ws,a3
	shl	$wl,d0
	sub	d0,a3
	movl	a3,(a2)
	addl	$ws,a2

copy_record_node2_3_ab_:
	movl	a0,(a3)
	lea	1(a3),a0
	movl	a0,(a1)

	movl	2*ws(a1),a0
	movl	ws(a1),a1
	movl	a1,ws(a3)
	lea	3*ws(a3),a4
	movl	a4,2*ws(a3)

	movl	(a0),a1
	movl	a1,(a4)
	addl	$ws,a4
	lea	3*ws+1(a3),a1
	movl	a1,(a0)
	addl	$ws,a0

cp_record_arg_lp2_3_ab:
	movl	(a0),a1
	addl	$ws,a0
	movl	a1,(a4)
	addl	$ws,a4
	subl	$ws,d0
	jne	cp_record_arg_lp2_3_ab

	pop	a4

	jmp	copy_lp3

record_arguments_already_copied2_3_ab:
	movl	a0,-3*ws(a3)
	movl	2*ws(a1),a0

	pop	d1
	pop	a4

	movl	(a0),a0
	jmp	arguments_already_copied_2_

copy_record_node2_3_b:
	push	a4
	push	d1

	movl	2*ws(a1),d1
	subl	heap_p1,d1
	mov	d1,a4
	shr	$wl+1,d1
	shr	$wl+4,a4
	and	$31,d1
	andl	$-4,a4
	mov	bit_set_table(,d1,4),d1
	addl	heap_copied_vector,a4
	test	(a4),d1
	jne	record_arguments_already_copied2_3_b

	or	d1,(a4)

	pop	d1

	sub	$3*ws+ws,a3
	shl	$wl,d0
	subl	d0,a3
	movl	a3,(a2)
	addl	$ws,a2

copy_record_node2_3_b_:
	movl	a0,(a3)
	lea	1(a3),a0
	movl	a0,(a1)

	movl	2*ws(a1),a0
	movl	ws(a1),a1
	movl	a1,ws(a3)
	lea	3*ws(a3),a4
	movl	a4,2*ws(a3)

	movl	(a0),a1
	movl	a1,(a4)
	add	$ws,a4
	lea	3*ws+1(a3),a1
	movl	a1,(a0)
	addl	$ws,a0

cp_record_arg_lp3_b:
	movl	(a0),a1
	addl	$ws,a0
	movl	a1,(a4)
	addl	$ws,a4
	subl	$ws,d0
	jne	cp_record_arg_lp3_b

	pop	a4

	subl	$1,d1
	jae	copy_lp2
	ret

record_arguments_already_copied2_3_b:
	movl	a0,-3*ws(a3)
	movl	2*ws(a1),a0

	popl	d1
	pop	a4

	movl	(a0),a0

	lea	-3*ws+1(a3),d0
	sub	$3*ws,a3
	mov	d0,(a1)
	mov	ws(a1),a1
	subl	$1,a0
	movl	a3,(a2)
	addl	$ws,a2
	movl	a1,ws(a3)
	movl	a0,2*ws(a3)

	subl	$1,d1
	jae	copy_lp2
	ret

not_in_hnf_2:
	testb	$1,a0b
	jne	already_copied_2

not_in_hnf_2_:
	mov	-4(a0),d0
	test	d0,d0
	jle	copy_arity_0_node2_

copy_node2_1_:
	cmpl	$257,d0
	jge	copy_unboxed_closure2

	movzbl	d0b,d0

	sub	$2,d0
	jl	copy_arity_1_node2
copy_node2_3:
	mov	a4,(a2)
	add	$ws,a2

copy_node2_3_:
	mov	a0,(a4)
	inc	a4
	mov	a4,(a1)
	mov	ws(a1),a0
	add	$2*ws,a1
	mov	a0,ws-1(a4)
	add	$2*ws-1,a4

cp_arg_lp2:
	mov	(a1),a0
	add	$ws,a1
	mov	a0,(a4)
	add	$ws,a4
	sub	$1,d0
	jae	cp_arg_lp2
	
	sub	$1,d1
	jae	copy_lp2
	ret

copy_unboxed_closure2:
	sub	$1,d0
	cmpb	-3(a0),d0b
	jb	copy_unboxed_closure2_b_
	je	copy_unboxed_closure2_ab_
	sub	$1,d0
	movzbl	d0b,d0
	jmp	copy_node2_3

copy_unboxed_closure2_b_:
	movzbl	d0b,d0
	test	d0,d0
	je	copy_unboxed_closure2_b_1

	sub	$2*ws,a3
	shl	$wl,d0
	sub	d0,a3
	movl	a3,(a2)
	add	$ws,a2

	push	d1

copy_unboxed_closure2_b__:
	movl	a0,(a3)

	lea	1(a3),a0
	movl	a0,(a1)

	movl	ws(a1),d1
	add	$2*ws,a1
	lea	2*ws(a3),a0
	movl	d1,ws(a3)

cp_closure_lp2_b:
	movl	(a1),d1
	add	$ws,a1
	movl	d1,(a0)
	add	$ws,a0
	sub	$ws,d0
	jne	cp_closure_lp2_b

	pop	d1

	subl	$1,d1
	jae	copy_lp2
	ret

copy_unboxed_closure2_b_1:
	lea	-3*ws+1(a3),d0
	mov	a0,-3*ws(a3)
	sub	$3*ws,a3
	mov	d0,(a1)
	mov	ws(a1),d0
	mov	a3,(a2)
	add	$ws,a2
	mov	d0,ws(a3)
	sub	$1,d1
	jae	copy_lp2
	ret

copy_unboxed_closure2_ab_:
	movzbl	d0b,d0

	sub	$2*ws,a3
	shl	$wl,d0
	sub	d0,a3
	movl	a3,(a2)
	add	$ws,a2

	push	d1

copy_unboxed_closure2_ab__:
	movl	a0,(a3)
	lea	1(a3),a0
	movl	a0,(a1)

	movl	ws(a1),d1
	add	$2*ws,a1
	lea	2*ws(a3),a0
	movl	d1,ws(a3)

cp_closure_lp2_ab:
	movl	(a1),d1
	add	$ws,a1
	movl	d1,(a0)
	add	$ws,a0
	sub	$ws,d0
	jne	cp_closure_lp2_ab

	pop	d1
	jmp	copy_lp3

copy_arity_1_node2__:
	pop	d1
copy_arity_1_node2_:
#ifdef PROFILE_GRAPH
	mov	2*ws(a1),d0
	mov	d0,-ws(a3)
#endif
copy_arity_1_node2:
	lea	-3*ws+1(a3),d0
	mov	a0,-3*ws(a3)
	sub	$3*ws,a3
	mov	d0,(a1)
	mov	ws(a1),a1
	mov	a3,(a2)
	add	$ws,a2
	mov	a1,ws(a3)
	jmp	copy_lp3_

copy_indirection_2:
	mov	a1,d0
	mov	ws(a1),a1

	mov	(a1),a0
	testb	$2,a0b
	jne	in_hnf_2

	testb	$1,a0b
	jne	already_copied_2

	cmpl	$-2,-4(a0)
	jne	not_in_hnf_2_

skip_indirections_2:
	mov	ws(a1),a1

	mov	(a1),a0
	testb	$3,a0b
	jne	update_indirection_list_2

	cmpl	$-2,-4(a0)
	je	skip_indirections_2

update_indirection_list_2:
	lea	ws(d0),a0
	mov	ws(d0),d0
	mov	a1,(a0)
	cmp	d0,a1
	jne	update_indirection_list_2

	jmp	continue_after_selector_2

copy_selector_2:
	cmpl	$-2,d0
	je	copy_indirection_2
	jl	copy_record_selector_2

	mov	ws(a1),d0
	push	d1

	mov	(d0),d1
 	testb	$2,d1b
	je	copy_arity_1_node2__

	cmpw	$2,-2(d1)
	jbe	copy_selector_2_

	movl	2*ws(d0),d1
	testb	$1,(d1)
	jne	copy_arity_1_node2__

	movl	-2*ws(a0),a0

	movzwl	4(a0),a0
	movl	$e__system__nind,(a1)

	cmpl	$2*ws,a0
	jl	copy_selector_2_1
	je	copy_selector_2_2

	movl	-3*ws(d1,a0),a0
	pop	d1
	movl	a0,ws(a1)
	movl	a0,a1
	jmp	continue_after_selector_2

copy_selector_2_1:
	movl	ws(d0),a0
	pop	d1
	movl	a0,ws(a1)
	movl	a0,a1
	jmp	continue_after_selector_2

copy_selector_2_2:
	movl	(d1),a0
	pop	d1
	movl	a0,ws(a1)
	movl	a0,a1
	jmp	continue_after_selector_2
	
copy_selector_2_:
	movl	-2*ws(a0),a0
	pop	d1

	movzwl	4(a0),a0
	movl	$e__system__nind,(a1)

	movl	(d0,a0),a0
	movl	a0,ws(a1)
	movl	a0,a1
	jmp	continue_after_selector_2

copy_record_selector_2:
	cmpl	$-3,d0
	movl	ws(a1),d0
	movl	(d0),d0
	je	copy_strict_record_selector_2

 	testb	$2,d0b
	je	copy_arity_1_node2_

	cmpw	$258,-2(d0)
	jbe	copy_record_selector_2_0

	cmpw	$2,-2+2(d0)
	jae	copy_record_selector_2_1

	movl	ws(a1),d0
	pushl	a1
	
	movl	2*ws(d0),d0
	subl	heap_p1,d0
	mov	d0,a1
	and	$31*8,d0
	shr	$6,a1
	shr	$1,d0
	andl	$-4,a1
	addl	heap_copied_vector,a1
	mov	bit_set_table(d0),d0
	andl	(a1),d0

	popl	a1

	je	copy_record_selector_2_0
	jmp	copy_arity_1_node2_

copy_record_selector_2_1:
	mov	ws(a1),d0
	mov	2*ws(d0),d0
	testb	$1,(d0)
	jne	copy_arity_1_node2_

copy_record_selector_2_0:
	movl	-2*ws(a0),d0
	movl	ws(a1),a0
	movl	$e__system__nind,(a1)

	movzwl	4(d0),d0
	cmpl	$2*ws,d0
	jle	copy_record_selector_2_2
	movl	2*ws(a0),a0
	subl	$3*ws,d0
copy_record_selector_2_2:
	movl	(a0,d0),a0

	movl	a0,ws(a1)

	movl	a0,a1
	jmp	continue_after_selector_2

copy_strict_record_selector_2:
	testb	$2,d0b
	je	copy_arity_1_node2_

	cmpw	$258,-2(d0)
	jbe	copy_strict_record_selector_2_0

	cmpw	$2,-2+2(d0)
	jb	copy_strict_record_selector_2_b

	movl	ws(a1),d0
	movl	2*ws(d0),d0
	testb	$1,(d0)
	jne	copy_arity_1_node2_

	jmp	copy_strict_record_selector_2_0

copy_strict_record_selector_2_b:

	movl	ws(a1),d0
	pushl	a1
	
	movl	2*ws(d0),d0
	subl	heap_p1,d0
	mov	d0,a1
	and	$31*8,d0
	shr	$6,a1
	shr	$1,d0
	andl	$-4,a1
	addl	heap_copied_vector,a1
	mov	bit_set_table(d0),d0
	and	(a1),d0

	popl	a1

	jne	copy_arity_1_node2_

copy_strict_record_selector_2_0:
	movl	-2*ws(a0),d0

	push	d1
	movl	ws(a1),a0

	movzwl	4(d0),d1
	cmpl	$2*ws,d1
	jle	copy_strict_record_selector_2_1
	addl	2*ws(a0),d1
	movl	-3*ws(d1),d1
	jmp	copy_strict_record_selector_2_2
copy_strict_record_selector_2_1:
	movl	(a0,d1),d1
copy_strict_record_selector_2_2:
	movl	d1,4(a1)

	movzwl	6(d0),d1
	testl	d1,d1
	je	copy_strict_record_selector_2_4
	cmpl	$2*ws,d1
	jle	copy_strict_record_selector_2_3
	movl	2*ws(a0),a0
	subl	$3*ws,d1
copy_strict_record_selector_2_3:
	movl	(a0,d1),d1
	movl	d1,2*ws(a1)
copy_strict_record_selector_2_4:

	movl	-4(d0),a0
	movl	a0,(a1)
	pop	d1
	testb	$2,a0b
	jne	in_hnf_2
	hlt

copy_arity_0_node2_:
	jl	copy_selector_2

	mov	a0,-3*ws(a3)
	sub	$3*ws,a3
	mov	a3,(a2)
	lea	1(a3),d0

	add	$ws,a2
	mov	d0,(a1)

	sub	$1,d1
	jae	copy_lp2
	ret

copy_string_or_array_2:
#ifdef DLL
	je	copy_string_2
	cmpl	$__ARRAY__+2,a0
	jb	copy_normal_hnf_0_2
	jmp	copy_array_2
copy_string_2:
#else
	jne	copy_array_2
#endif
	movl	neg_heap_p1,d0
	addl	a1,d0
	cmp	semi_space_size,d0
	jae	copy_string_or_array_constant

	movl	a1,a0
	mov	ws(a1),a1
	add	$ws,a2

	push	d1

	add	$ws-1,a1
	mov	a1,d0
	and	$-ws,a1
	shr	$wl,d0
	sub	a1,a3
	sub	$2*ws,a3
	mov	a3,-ws(a2)

copy_string_2_:
	mov	(a0),d1
	add	$ws,a0
	mov	d1,(a3)
	lea	1(a3),a1
	mov	a1,-ws(a0)
	lea	ws(a3),a1

cp_s_arg_lp2:
	mov	(a0),d1
	add	$ws,a0
	mov	d1,(a1)
	add	$ws,a1
	subl	$1,d0
	jge	cp_s_arg_lp2

	pop	d1
	sub	$1,d1
	jae	copy_lp2
	ret

copy_array_2:
	movl	neg_heap_p1,d0
	addl	a1,d0
	cmp	semi_space_size,d0
	jae	copy_string_or_array_constant

	push	d1

	cmpl	$__ARRAY__R__+2,a0
	ja	copy_strict_basic_array_2
	jb	copy_array_a2

	movl	2*ws(a1),d0
	movzwl 	-2(d0),d1
	subl	$256,d1
	imull	ws(a1),d1

	cmpw	$0,-2+2(d0)
	je	copy_array_rb2

	movl	a0,(a4)
	movl	a1,a0

	movl	a4,a1
	lea	3*ws(a4,d1,4),a4

	lea	1(d1),d0
	jmp	copy_array_2_

copy_array_rb2:
	sub	$3*ws,a3
	lea	1(d1),d0

	shl	$wl,d1
	subl	d1,a3

	movl	a0,(a3)
	movl	a1,a0
	movl	a3,a1
	jmp	copy_array_2_

copy_array_a2:
	movl	ws(a1),d0

	movl	a0,(a4)

	cmpl	$__ARRAY__+2,a0
	jne	copy_arrayp2_a2

	movl	a1,a0
	movl	a4,a1
	lea	2*ws(a4,d0,4),a4
	jmp	copy_array_2_

copy_arrayp2_a2:
	cmp	$1,d0
	jbe	copy_arrayp2_a2_01

	sub	$1,d0
	bsr	d0,a0
	mov	$2,d0
	shl	a0b,d0

	movl	a1,a0
	movl	a4,a1
	lea	2*ws(a4,d0,4),a4

	mov	ws(a0),d0
	jmp	copy_array_2_

copy_arrayp2_a2_01:
	movl	a1,a0
	movl	a4,a1
	lea	2*ws(a4,d0,8),a4
	jmp	copy_array_2_

copy_strict_basic_array_2:
	movl	ws(a1),d1

	cmpl	$__ARRAY__REAL__+2,a0
	je	copy_real_array_2
	cmpl	$__ARRAY__BOOL__+2,a0
	ja	copy_unboxed_basic_arrayp2_2
	je	copy_bool_array_2

copy_int_array_2:
	sub	$2*ws,a3
	movl	d1,d0
	shl	$wl,d1
	subl	d1,a3

copy_arrayp2_2:
	movl	a0,(a3)
	movl	a1,a0
	movl	a3,a1

copy_array_2_:
	movl	a1,(a2)
	addl	$ws,a2
copy_array_2__:
	lea	1(a1),d1
	addl	$ws,a1
	movl	d1,(a0)
	addl	$ws,a0
	jmp	cp_s_arg_lp2

copy_string_or_array_constant:
	movl	a1,(a2)
	add	$ws,a2

	sub	$1,d1
	jae	copy_lp2
	ret

copy_bool_array_2:
	add	$ws-1,d1
	shr	$wl,d1
	jmp	copy_int_array_2

copy_real_array_2:
	addl	d1,d1
	jmp	copy_int_array_2

copy_unboxed_basic_arrayp2_2:
	cmpl	$__ARRAYP2__REAL__+2,a0
	je	copy_real_arrayp2_2
	cmpl	$__ARRAYP2__BOOL__+2,a0
	jae	copy_bool_or_char_arrayp2_2

copy_int_or_int32_or_real32_arrayp2_2:
	cmp	$1,d1
	jbe	copy_int_or_int32_or_real32_arrayp2_2_01

	lea	-1(d1),a0
	mov	$2*ws,d0

copy_unboxed_basic_arrayp2_2_:
	bsr	a0,a0
	shl	a0b,d0

	movl	(a1),a0

	sub	$2*ws,a3
	sub	d0,a3

	mov	d1,d0
	jmp	copy_arrayp2_2

copy_int_or_int32_or_real32_arrayp2_2_01:
	mov	d1,d0
	shl	$wl+1,d1
	sub	$2*ws,a3
	sub	d1,a3
	jmp	copy_arrayp2_2

copy_real_arrayp2_2:
	cmp	$1,d1
	jbe	copy_real_arrayp2_2_01

	lea	-1(d1),a0
	add	d1,d1
	mov	$4*ws,d0
	jmp	copy_unboxed_basic_arrayp2_2_

copy_real_arrayp2_2_01:
	mov	d1,d0
	shl	$wl+2,d1
	sub	$2*ws,a3
	add	d0,d0
	sub	d1,a3
	jmp	copy_arrayp2_2

copy_bool_or_char_arrayp2_2:
	cmp	$8,d1
	jbe	copy_bool_array_2

	lea	-1(d1),a0
	bsr	a0,a0
	mov	$2,d0
	shl	a0b,d0

	movl	(a1),a0

	sub	$2*ws,a3
	sub	d0,a3

	lea	ws-1(d1),d0
	shr	$wl,d0
	jmp	copy_arrayp2_2

copy_lp3:
	movl	ws(a3),a1

copy_lp3_:
continue_after_selector_3:
	movl	(a1),a0
	testb	$2,a0b
	je	not_in_hnf_3

in_hnf_3:
	movzwl	-2(a0),d0
	test	d0,d0
	je	copy_arity_0_node3

	cmp	$256,d0
	jae	copy_record_3

	sub	$2,d0
	jb	copy_hnf_node3_1
	ja	copy_hnf_node3_3

	mov	a4,ws(a3)
	jmp	copy_hnf_node2_2_

copy_hnf_node3_1:
	lea	-2*ws+1(a3),d0
	mov	a0,-2*ws(a3)
	sub	$2*ws,a3
	mov	d0,(a1)
	mov	ws(a1),a1
	mov	a3,3*ws(a3)
	mov	a1,ws(a3)
	jmp	copy_lp3_

copy_hnf_node3_3:
	push	d1

	mov	2*ws(a1),d1

	testl	$1,(d1)
	jne	arguments_already_copied_3

	mov	a4,ws(a3)
	jmp	copy_hnf_node2_3_

arguments_already_copied_3:
	mov	a0,-3*ws(a3)
	mov	(d1),a0

	pop	d1

arguments_already_copied_3_:
	lea	-3*ws+1(a3),d0
	sub	$3*ws,a3
	mov	d0,(a1)
	mov	ws(a1),a1
	sub	$1,a0
	mov	a3,4*ws(a3)
	mov	a1,ws(a3)
	mov	a0,2*ws(a3)
	jmp	copy_lp3_

copy_arity_0_node3:
	cmp	$INT+2,a0
	jb	copy_real_file_or_string_3

	cmp	$CHAR+2,a0
	ja	copy_normal_hnf_0_3

copy_int_bool_or_char_3:
	mov	ws(a1),d0
	je	copy_char_3

	cmp	$INT+2,a0
	jne	no_small_int_or_char_3

copy_int_3:
	cmp	$33,d0
	jae	no_small_int_or_char_3

	shl	$3,d0
	add	$small_integers,d0
	sub	$1,d1
	mov	d0,ws(a3)
	jae	copy_lp2
	ret

copy_char_3:
	movzbl	d0b,d0
	shl	$3,d0
	add	$static_characters,d0
	sub	$1,d1
	mov	d0,ws(a3)
	jae	copy_lp2
	ret

no_small_int_or_char_3:

copy_record_node3_1_b:
	mov	a0,-2*ws(a3)
	lea	-2*ws+1(a3),a0
	mov	d0,-ws(a3)
	sub	$2*ws,a3
	mov	a0,(a1)
	mov	a3,3*ws(a3)
	sub	$1,d1
	jae	copy_lp2
	ret

copy_normal_hnf_0_3:
	sub	$2-ZERO_ARITY_DESCRIPTOR_OFFSET,a0
	sub	$1,d1
	mov	a0,ws(a3)
	jae	copy_lp2
	ret

copy_real_file_or_string_3:
	cmpl	$__STRING__+2,a0
	jbe	copy_string_or_array_3

copy_real_or_file_3:
	mov	a0,-3*ws(a3)
	lea	-3*ws+1(a3),a0
	mov	a0,(a1)
	sub	$3*ws,a3
	mov	ws(a1),d0
	mov	2*ws(a1),a0
	mov	a3,4*ws(a3)
	mov	d0,ws(a3)
	sub	$1,d1
	mov	a0,2*ws(a3)
	jae	copy_lp2
	ret

already_copied_3:
	dec	a0
	sub	$1,d1
	mov	a0,ws(a3)
	jae	copy_lp2
	ret

copy_record_3:
	subl	$258,d0
	ja	copy_record_node3_3
	jb	copy_record_node3_1

	cmpw	$1,-2+2(a0)
	jb	copy_real_or_file_3
	je	copy_record_node3_2ab

	mov	a4,ws(a3)
	jmp	copy_hnf_node2_2_

copy_record_node3_2ab:
	lea	-3*ws+1(a3),d0
	mov	a0,-3*ws(a3)
	sub	$3*ws,a3
	mov	2*ws(a1),a0
	mov	d0,(a1)
	mov	ws(a1),a1
	mov	a3,4*ws(a3)
	mov	a0,2*ws(a3)
	mov	a1,ws(a3)
	jmp	copy_lp3_

copy_record_node3_1:
	movl	ws(a1),d0

	cmpw	$0,-2+2(a0)
	je	copy_record_node3_1_b

copy_record_node3_1_a:
	mov	a0,-2*ws(a3)
	lea	-2*ws+1(a3),a0
	sub	$2*ws,a3
	mov	a0,(a1)
	mov	d0,a1
	mov	a3,3*ws(a3)
	mov	a1,ws(a3)
	jmp	copy_lp3_

copy_record_node3_3:
	cmpw	$1,-2+2(a0)
	ja	copy_hnf_node3_3
	jb	copy_record_node3_3_b

copy_record_node3_3_ab:
	push	a4
	push	d1

	movl	2*ws(a1),d1
	subl	heap_p1,d1
	mov	d1,a4
	shr	$wl+1,d1
	shr	$wl+4,a4
	and	$31,d1
	andl	$-4,a4
	mov	bit_set_table(,d1,4),d1
	addl	heap_copied_vector,a4
	test	(a4),d1
	jne	record_arguments_already_copied3_3_ab

	or	d1,(a4)

	pop	d1

	mov	a3,a4
	sub	$3*ws+ws,a3
	shl	$2,d0
	sub	d0,a3
	movl	a3,ws(a4)
	jmp	copy_record_node2_3_ab_

record_arguments_already_copied3_3_ab:
	movl	a0,-3*ws(a3)
	movl	2*ws(a1),a0

	pop	d1
	pop	a4

	movl	(a0),a0
	jmp	arguments_already_copied_3_

copy_record_node3_3_b:
	push	a4
	push	d1

	movl	2*ws(a1),d1
	subl	heap_p1,d1
	mov	d1,a4
	shr	$wl+1,d1
	shr	$wl+4,a4
	and	$31,d1
	andl	$-4,a4
	mov	bit_set_table(,d1,4),d1
	addl	heap_copied_vector,a4
	test	(a4),d1
	jne	record_arguments_already_copied3_3_b

	or	d1,(a4)

	pop	d1

	mov	a3,a4
	sub	$3*ws+ws,a3
	shl	$2,d0
	sub	d0,a3
	movl	a3,ws(a4)
	jmp	copy_record_node2_3_b_

record_arguments_already_copied3_3_b:
	movl	a0,-3*ws(a3)
	movl	2*ws(a1),a0

	pop	d1
	pop	a4

	movl	(a0),a0

	lea	-3*ws+1(a3),d0
	sub	$3*ws,a3
	mov	d0,(a1)
	mov	ws(a1),a1
	sub	$1,a0
	mov	a3,4*ws(a3)
	mov	a1,ws(a3)
	mov	a0,2*ws(a3)

	subl	$1,d1
	jae	copy_lp2
	ret

not_in_hnf_3:
	testb	$1,a0b
	jne	already_copied_3

not_in_hnf_3_:
	mov	-4(a0),d0
	test	d0,d0
	jle	copy_arity_0_node3_

	cmpl	$257,d0
	jge	copy_unboxed_closure3

	movzbl	d0b,d0
	sub	$2,d0
	jl	copy_arity_1_node3

copy_node3_3:
	mov	a4,ws(a3)
	jmp	copy_node2_3_

copy_unboxed_closure3:
	sub	$1,d0
	cmpb	-3(a0),d0b
	jb	copy_unboxed_closure3_b_
	je	copy_unboxed_closure3_ab_
	movzbl	d0b,d0
	sub	$1,d0
	mov	a4,ws(a3)
	jmp	copy_node2_3_

copy_unboxed_closure3_b_:
	movzbl	d0b,d0
	test	d0,d0
	je	copy_unboxed_closure3_b_1

	push	d1

	mov	a3,d1
	sub	$2*ws,a3
	shl	$2,d0
	sub	d0,a3
	movl	a3,ws(d1)
	jmp	copy_unboxed_closure2_b__

copy_unboxed_closure3_b_1:
	lea	-3*ws+1(a3),d0
	mov	a0,-3*ws(a3)
	sub	$3*ws,a3
	mov	d0,(a1)
	mov	ws(a1),d0
	mov	a3,4*ws(a3)
	mov	d0,ws(a3)
	sub	$1,d1
	jae	copy_lp2
	ret

copy_unboxed_closure3_ab_:
	movzbl	d0b,d0

	push	d1

	mov	a3,d1
	sub	$2*ws,a3
	shl	$2,d0
	sub	d0,a3
	movl	a3,ws(d1)
	jmp	copy_unboxed_closure2_ab__

copy_arity_1_node3__:
	pop	d1
copy_arity_1_node3_:
#ifdef PROFILE_GRAPH
	mov	2*ws(a1),d0
	mov	d0,-ws(a3)
#endif
copy_arity_1_node3:
	lea	-3*ws+1(a3),d0
	mov	a0,-3*ws(a3)
	sub	$3*ws,a3
	mov	d0,(a1)
	mov	ws(a1),a1
	mov	a3,4*ws(a3)
	mov	a1,ws(a3)
	jmp	copy_lp3_

copy_indirection_3:
	mov	a1,d0
	mov	ws(a1),a1

	mov	(a1),a0
	testb	$2,a0b
	jne	in_hnf_3

	testb	$1,a0b
	jne	already_copied_3

	cmpl	$-2,-4(a0)
	jne	not_in_hnf_3_

skip_indirections_3:
	mov	ws(a1),a1

	mov	(a1),a0
	testb	$3,a0b
	jne	update_indirection_list_3

	cmpl	$-2,-4(a0)
	je	skip_indirections_3

update_indirection_list_3:
	lea	ws(d0),a0
	mov	ws(d0),d0
	mov	a1,(a0)
	cmp	d0,a1
	jne	update_indirection_list_3

	jmp	continue_after_selector_3

copy_selector_3:
	cmpl	$-2,d0
	je	copy_indirection_3
	jl	copy_record_selector_3

	mov	ws(a1),d0
	push	d1

	mov	(d0),d1
	testb	$2,d1b
	je	copy_arity_1_node3__

	cmpw	$2,-2(d1)
	jbe	copy_selector_3_

	movl	2*ws(d0),d1
	testb	$1,(d1)
	jne	copy_arity_1_node3__

	movl	-2*ws(a0),a0

	movzwl	ws(a0),a0
	movl	$e__system__nind,(a1)

	cmpl	$2*ws,a0
	jl	copy_selector_3_1
	je	copy_selector_3_2

	movl	-3*ws(d1,a0),a0
	pop	d1
	movl	a0,ws(a1)
	movl	a0,a1
	jmp	continue_after_selector_3

copy_selector_3_1:
	movl	ws(d0),a0
	pop	d1
	movl	a0,ws(a1)
	movl	a0,a1
	jmp	continue_after_selector_3

copy_selector_3_2:
	movl	(d1),a0
	pop	d1
	movl	a0,ws(a1)
	movl	a0,a1
	jmp	continue_after_selector_3

copy_selector_3_:
	movl	-2*ws(a0),a0
	pop	d1

	movzwl	ws(a0),a0
	movl	$e__system__nind,(a1)

	movl	(d0,a0),a0
	movl	a0,ws(a1)
	movl	a0,a1
	jmp	continue_after_selector_3

copy_record_selector_3:
	cmpl	$-3,d0
	movl	ws(a1),d0
	movl	(d0),d0
	je	copy_strict_record_selector_3

	testb	$2,d0b
	je	copy_arity_1_node3_

	cmpw	$258,-2(d0)
	jbe	copy_record_selector_3_0

	cmpw	$2,-2+2(d0)
	jae	copy_record_selector_3_1

	movl	ws(a1),d0
	pushl	a1

	movl	2*ws(d0),d0
	subl	heap_p1,d0
	mov	d0,a1
	and	$31*8,d0
	shr	$6,a1
	shr	$1,d0
	andl	$-4,a1
	addl	heap_copied_vector,a1
	mov	bit_set_table(d0),d0
	andl	(a1),d0

	popl	a1

	je	copy_record_selector_3_0
	jmp	copy_arity_1_node3_

copy_record_selector_3_1:
	mov	ws(a1),d0
	mov	2*ws(d0),d0
	testb	$1,(d0)
	jne	copy_arity_1_node3_

copy_record_selector_3_0:
	movl	-2*ws(a0),d0
	movl	ws(a1),a0
	movl	$e__system__nind,(a1)

	movzwl	4(d0),d0
	cmpl	$2*ws,d0
	jle	copy_record_selector_3_2
	movl	2*ws(a0),a0
	subl	$3*ws,d0
copy_record_selector_3_2:
	movl	(a0,d0),a0

	movl	a0,ws(a1)

	movl	a0,a1
	jmp	continue_after_selector_3

copy_strict_record_selector_3:
	testb	$2,d0b
	je	copy_arity_1_node3_

	cmpw	$258,-2(d0)
	jbe	copy_strict_record_selector_3_0

	cmpw	$2,-2+2(d0)
	jb	copy_strict_record_selector_3_b

	movl	ws(a1),d0
	movl	2*ws(d0),d0
	testb	$1,(d0)
	jne	copy_arity_1_node3_

	jmp	copy_strict_record_selector_3_0

copy_strict_record_selector_3_b:
	movl	ws(a1),d0

	pushl	a1

	movl	2*ws(d0),d0
	subl	heap_p1,d0
	mov	d0,a1
	and	$31*8,d0
	shr	$6,a1
	shr	$1,d0
	andl	$-4,a1
	addl	heap_copied_vector,a1
	mov	bit_set_table(d0),d0
	and	(a1),d0

	popl	a1

	jne	copy_arity_1_node3_

copy_strict_record_selector_3_0:
	movl	-2*ws(a0),d0

	push	d1
	movl	ws(a1),a0

	movzwl	4(d0),d1
	cmpl	$2*ws,d1
	jle	copy_strict_record_selector_3_1
	addl	2*ws(a0),d1
	movl	-3*ws(d1),d1
	jmp	copy_strict_record_selector_3_2
copy_strict_record_selector_3_1:
	movl	(a0,d1),d1
copy_strict_record_selector_3_2:
	movl	d1,ws(a1)

	movzwl	6(d0),d1
	testl	d1,d1
	je	copy_strict_record_selector_3_4
	cmpl	$2*ws,d1
	jle	copy_strict_record_selector_3_3
	movl	2*ws(a0),a0
	subl	$3*ws,d1
copy_strict_record_selector_3_3:
	movl	(a0,d1),d1
	movl	d1,2*ws(a1)
copy_strict_record_selector_3_4:

	movl	-4(d0),a0
	movl	a0,(a1)
	pop	d1
	testb	$2,a0b
	jne	in_hnf_3
	hlt

copy_arity_0_node3_:
	jl	copy_selector_3

	mov	a0,-3*ws(a3)
	lea	-3*ws+1(a3),d0
	sub	$3*ws,a3
	mov	d0,(a1)
	mov	a3,4*ws(a3)

	sub	$1,d1
	jae	copy_lp2
	ret

copy_string_or_array_3:
#ifdef DLL
	je	copy_string_3
	cmpl	$__ARRAY__+2,a0
	jb	copy_normal_hnf_0_3
	jmp	copy_array_3
copy_string_3:
#else
	jne	copy_array_3
#endif
	movl	neg_heap_p1,d0
	addl	a1,d0
	cmp	semi_space_size,d0
	jae	copy_string_or_array_constant_3

	movl	a1,a0
	mov	ws(a1),a1

	push	d1

	mov	a3,d1
	add	$ws-1,a1
	mov	a1,d0
	and	$-ws,a1
	shr	$2,d0
	sub	a1,a3
	sub	$2*ws,a3
	mov	a3,ws(d1)
	jmp	copy_string_2_

copy_array_3:
	movl	neg_heap_p1,d0
	addl	a1,d0
	cmp	semi_space_size,d0
	jae	copy_string_or_array_constant_3

	push	d1

	cmpl	$__ARRAY__R__+2,a0
	ja	copy_strict_basic_array_3
	jb	copy_array_a3

	movl	2*ws(a1),d0
	movzwl 	-2(d0),d1
	subl	$256,d1
	imull	ws(a1),d1

	cmpw	$0,-2+2(d0)
	je	copy_array_rb3

	movl	a0,(a4)
	movl	a1,a0
	movl	a4,a1
	lea	3*ws(a4,d1,4),a4
	lea	1(d1),d0
	movl	a1,ws(a3)
	jmp	copy_array_2__

copy_array_rb3:
	sub	$3*ws,a3
	lea	1(d1),d0
	shl	$2,d1
	neg	d1

	movl	a0,(a3,d1)
	mov	a1,a0
	add	a3,d1

	movl	d1,3*ws+ws(a3)
	mov	d1,a1
	mov	d1,a3
	jmp	copy_array_2__

copy_array_a3:
	movl	ws(a1),d0

	movl	a0,(a4)

	cmpl	$__ARRAY__+2,a0
	jne	copy_arrayp2_a3

	movl	a1,a0
	movl	a4,a1
	lea	2*ws(a4,d0,4),a4

	movl	a1,ws(a3)
	jmp	copy_array_2__

copy_arrayp2_a3:
	cmp	$1,d0
	jbe	copy_arrayp2_a3_01

	sub	$1,d0
	bsr	d0,a0
	mov	$2,d0
	shl	a0b,d0

	movl	a1,a0
	movl	a4,a1
	lea	2*ws(a4,d0,4),a4

	mov	ws(a0),d0

	mov	a1,ws(a3)
	jmp	copy_array_2__

copy_arrayp2_a3_01:
	movl	a1,a0
	movl	a4,a1
	lea	2*ws(a4,d0,8),a4

	mov	a1,ws(a3)
	jmp	copy_array_2__

copy_strict_basic_array_3:
	movl	ws(a1),d1

	cmpl	$__ARRAY__REAL__+2,a0
	je	copy_real_array_3
	cmpl	$__ARRAY__BOOL__+2,a0
	ja	copy_unboxed_basic_arrayp2_3
	je	copy_bool_array_3

copy_int_array_3:
	sub	$2*ws,a3
	mov	d1,d0
	shl	$2,d1
	neg	d1

	movl	a0,(a3,d1)
	mov	a1,a0
	add	a3,d1

	movl	d1,2*ws+ws(a3)
	mov	d1,a1
	mov	d1,a3
	jmp	copy_array_2__

copy_string_or_array_constant_3:
	movl	a1,ws(a3)

	sub	$1,d1
	jae	copy_lp2
	ret

copy_bool_array_3:
	add	$ws-1,d1
	shr	$wl,d1
	jmp	copy_int_array_3

copy_real_array_3:
	addl	d1,d1
	jmp	copy_int_array_3

copy_unboxed_basic_arrayp2_3:
	cmpl	$__ARRAYP2__REAL__+2,a0
	je	copy_real_arrayp2_3
	cmpl	$__ARRAYP2__BOOL__+2,a0
	jae	copy_bool_or_char_arrayp2_3

copy_int_or_int32_or_real32_arrayp2_3:
	cmp	$1,d1
	jbe	copy_int_or_int32_or_real32_arrayp2_3_01

	lea	-1(d1),a0
	mov	$-2*ws,d0

copy_unboxed_basic_arrayp2_3_:
	bsr	a0,a0
	shl	a0b,d0

	movl	(a1),a0

copy_unboxed_basic_arrayp2_3__:
	sub	$2*ws,a3
	movl	a0,(a3,d0)
	add	a3,d0
	mov	a1,a0

	movl	d0,2*ws+ws(a3)
	mov	d0,a1
	mov	d0,a3

	mov	d1,d0
	jmp	copy_array_2__

copy_int_or_int32_or_real32_arrayp2_3_01:
	mov	d1,d0
	shl	$wl+1,d0
	neg	d0
	jmp	copy_unboxed_basic_arrayp2_3__

copy_real_arrayp2_3:
	cmp	$1,d1
	jbe	copy_real_arrayp2_3_01

	lea	-1(d1),a0
	add	d1,d1
	mov	$-4*ws,d0
	jmp	copy_unboxed_basic_arrayp2_3_

copy_real_arrayp2_3_01:
	mov	d1,d0
	add	d1,d1
	shl	$wl+2,d0
	neg	d0
	jmp	copy_unboxed_basic_arrayp2_3__

copy_bool_or_char_arrayp2_3:
	cmp	$8,d1
	jbe	copy_bool_array_3

	lea	-1(d1),a0
	bsr	a0,a0
	mov	$-2,d0
	shl	a0b,d0

	movl	(a1),a0

	sub	$2*ws,a3
	movl	a0,(a3,d0)
	add	a3,d0
	mov	a1,a0

	movl	d0,2*ws+ws(a3)
	mov	d0,a1
	mov	d0,a3

	lea	ws-1(d1),d0
	shr	$wl,d0
	jmp	copy_array_2__

end_copy1:
	mov	a3,heap_end_after_gc

#ifdef FINALIZERS
	movl	$finalizer_list,a0
	movl	$free_finalizer_list,a1
	movl	finalizer_list,a2

determine_free_finalizers_after_copy:
	movl	(a2),d0
	testb	$1,d0b
	je	finalizer_not_used_after_copy

	movl	ws(a2),a2
	subl	$1,d0
	movl	d0,(a0)
	lea	ws(d0),a0
	jmp	determine_free_finalizers_after_copy

finalizer_not_used_after_copy:
	cmpl	$__Nil-4,a2
	je	end_finalizers_after_copy

	movl	a2,(a1)
	lea	ws(a2),a1
	movl	ws(a2),a2
	jmp	determine_free_finalizers_after_copy	

end_finalizers_after_copy:
	movl	a2,(a0)
	movl	a2,(a1)
#endif

	lea	-32(a3),a2
	movl	a2,end_heap

